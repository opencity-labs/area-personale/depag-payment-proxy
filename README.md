# Depag payment proxy

Questo servizio si occupa di fare da intermediario tra la stanza del cittadino e il provider di pagamento Depag

## Stadio di sviluppo

Il servizio è nello stato di Development 1.0.8

## Come si usa

Da terminale:

1. `git clone https://gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy.git`
2. `cd depag-payment-proxy`
3. `docker-compose up -d` (assicurarsi che kafka sia su, in caso contrario digitare `docker-compose up -d kafka`)
4. Accedere alla documentazione delle API via `http://0.0.0.0:8000/docs`
5. Usare la `POST /tenants` e inserire la configurazione usando lo schema indicato
6. Usare la `POST /services` e inserire la configurazione usando lo schema indicato (inserire come tenant_id l'id del tenant precedentemente creato)
7. Accedere a `kafka-ui` via `http://localhost:8080`
8. Accedere al topic `payments` e andare nella sezione Messages
9. Copiare il messaggio presente nel topic
10. Premere "Produce Message",
11. Copiare il messaggio settando
    1. Il campo `id` ad un uuid valido
    2. Il campo `iud` all esadecimale dell'id dello step precedente (basta eliminare i trattini)
    3. Il campo `expire_at` ad una data successiva a quella odierna
    4. Il campo `tenant_id` all'id settato nello step 5
    5. Il campo `service_id` all'id settato nello step 6
12. Inviare il messaggio
13. Accedere alla documentazione delle API via `http://0.0.0.0:8000/docs
14. Usare la GET `/online-payment/{payment_id}` e inserire l'id settato allo step 11.1
15. Usare il link restituito per pagare

## Configurazione

### Configurazione variabili d'ambiente

| Nome                         | Default                                                                            | Descrizione                                                                             |
|------------------------------|------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|
| APP_NAME                     | depag-payment-proxy                                                                | Nome del microservizio                                                                  |
| DEBUG                        | true                                                                               | Modalità debug con più log disponibili                                                  |
| ---------------------------- | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| SENTRY_ENABLED               | true                                                                               | Attivare integrazione con Sentry?                                                       |
| SENTRY_TOKEN                 | https://c2628ad808302a86c6408da8fc9a9905@o348837.ingest.sentry.io/4505681073668096 | Token integrazione Sentry                                                               |
| ---------------------------- | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| KAFKA_BOOTSTRAP_SERVERS      | localhost:9092                                                                     | Endpoint di kafka                                                                       |
| KAFKA_GROUP_ID               | depag_payment_proxy                                                                | Nome del consumer group                                                                 |
| KAFKA_TOPIC_NAME             | payments                                                                           | Topic di kafka da cui si leggono i pagamenti da processare                              |
| ---------------------------- | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| HTTP_BIND                    | 0.0.0.0                                                                            | ....                                                                                    |
| HTTP_PORT                    | 40019                                                                              | ....                                                                                    |
| EXTERNAL_API_URL             | https://opencity.app.zoonect.dev                                                   | utilizzata dal cittadino per scaricare il bollettino ed effettuare il pagamento online  |
|                              |                                                                                    | (utilizzata anche dal provider come url per reinderizzare l'utente sulla Stanza Del     |
|                              |                                                                                    |  Cittadino)                                                                             |
| INTERNAL_API_URL             | Non ha valore di default. se null, il servizio non può funzionare                  | utilizzata internamente per eseguire l'update di un pagamento                           |
| BASEPATH                     | /payment-proxy/depag/                                                              | utilizzata per impostare il basepath del proxy                                          |
| ---------------------------- | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| CACHE_EXPIRATION             | 5m                                                                                 | Scadenza della cache                                                                    |
| CACHE_EVICTION               | 10m                                                                                | Periodo di pulizia degli elementi scaduti dalla cache                                   |
| ---------------------------- | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| STORAGE_TYPE                 | s3                                                                                 | Tipo di storage dei pagamenti: s3, azure, local                                         |
| STORAGE_BUCKET               | payments                                                                           | Nome dello storage S3 o AZURE dei pagamenti e delle configurazioni di tenant e servizio |
| STORAGE_CONFIGS_BASE_PATH    | sdc-payments/depag/tenants/                                                        | Path di salvataggio delle configurazioni di tenant e servizio                           |
| STORAGE_PAYMENTS_BASE_PATH   | sdc-payments/depag/payments/                                                       | Path di salvataggio dei pagamenti                                                       |
| ---------------------------- | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| STORAGE_S3_KEY               | 02tzwnQ8qYBRFdn1zzI8XwEXAMPLE                                                      | Chiave dello storage S3 dei pagamenti e delle configurazioni di tenant e servizio       |
| STORAGE_S3_SECRET            | 02tzwnWAIRCBJwA7HtHUnsEXAMPLEKEY                                                   | Password dello storage S3 dei pagamenti e delle configurazioni di tenant e servizio     |
| STORAGE_S3_REGION            | local                                                                              | Region dello storage S3 dei pagamenti e delle configurazioni di tenant e servizio       |
| STORAGE_S3_ENDPOINT          | localhost:9000                                                                     | Endpoint dello storage S3 dei pagamenti e della configurazioni di tenanto e servizio    |
| STORAGE_S3_SSL               | false                                                                              | SSL richiesto per storage S3 dei pagamenti e della configurazioni di tenanto e servizio |
| ---------------------------- | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| STORAGE_AZURE_ACCOUNT        | 02tzwnQ8qYBRFdn1zzI8XwEXAMPLE                                                      | Chiave dello storage AZURE dei pagamenti e delle configurazioni di tenant e servizio    |
| STORAGE_AZURE_KEY            | 02tzwnWAIRCBJwA7HtHUnsEXAMPLEKEY                                                   | Password dello storage S3 dei pagamenti e delle configurazioni di tenant e servizio     |
| ---------------------------- | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| STORAGE_LOCAL_PATH           | /data/payments                                                                     | Directory base per lo storage locale, le directory interne DEVONO già esistere          |
| ---------------------------- | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| DEPAG_WEBHOOK_USER           | opencontent                                                                        | Username auth basic per webhook notifiche pagamenti                                     |
| DEPAG_WEBHOOK_PASS           | test_opencontent                                                                   | Password auth basic per webhook notifiche pagamenti                                     |
| ---------------------------- | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |

### Configurazione Tenant (i valori sono di esempio)

```json
{
  "id": "b212c4b4-db26-4404-8c7c-47dab99dd2e6",
  "active": true,
  "endpoint": "http://domain.example.com",
  "production": true,
  "username": "username@domain.com",
  "password": "Password123!"
}
```

### Configurazione Servizio (i valori sono di esempio)

```json
{
  "id": "fd4c5bc6-5b05-4eca-8a30-bc8d0860296e",
  "tenant_id": "b212c4b4-db26-4404-8c7c-47dab99dd2e6",
  "active": true,
  "code": "0000088",
  "description": "Abbonamento aree di sosta",
  "due_application": "pagamenti",
  "due_type": "test"
}
```

### Struttura configurazioni tenants e services

La configurazione avviene automaticamente tramite compilazione form da UI.

Le cartelle contenenti le configurazioni dei tenant e dei servizi sono strutturate come segue:
* Cartella root: `/tenants`
* Cartella contenente le configurazioni del tenant e del servizio: `/tenants/{tenant_id}`
* File contenente la configurazione del tenant: `/tenants/{tenant_id}/tenant.json`
* File contenente la configurazione del servizio: `/tenants/{tenant_id}/{service_id}.json `

L'albero delle cartelle quindi è il seguente:

```
/tenants
    /{tenant_id_1}
        tenant_{tenant_id_1}.json
        service_{service_id_1}.json
        service_{service_id_2}.json
        ...
        service_{service_id_n}.json
    /{tenant_id_2}
        tenant_{tenant_id_2}.json
        service_{service_id_1}.json
        service_{service_id_2}.json
        ...
        service_{service_id_n}.json
    ...
    /{tenant_id_n}
        tenant_{tenant_id_n}.json
        service_{service_id_1}.json
        service_{service_id_2}.json
        ...
        service_{service_id_n}.json
```

La nomenclatura dei file e delle cartelle è la seguente:
* La cartella root va chiamata `tenants`
* La cartella contenente le configurazioni del tenant e del servizio va nominata con l'`id` del tenant considerato (`{tenant_id}`)
* Il file contenente la configurazione del tenant va nominato `tenant_{tenant_id}.json`
* Il file contenente la configurazione del servizio va nominato con l'`id` del servizio considerato (`service_{service_id}.json`)

## Sast

## Dependency Scanning

## Renovate

## Coding Style

### Altri comandi utili in sviluppo

    docker-compose -f docker-compose.dev.yml up

    go run main.go
    go run main.go > out.log 2> err.log

    go run tools/main.go generate tenant
    go run tools/main.go generate service -t "6361887b-56cb-4de9-a013-aa600326a911"
    go run tools/main.go generate service -t "6361887b-56cb-4de9-a013-aa600326a911" -b
    go run tools/main.go generate payment -t "6361887b-56cb-4de9-a013-aa600326a911" -s "97ee35cf-d288-4e24-8d03-6b0f7a583902"
    go run tools/main.go generate payment -t "6361887b-56cb-4de9-a013-aa600326a911" -s "97ee35cf-d288-4e24-8d03-6b0f7a583902" -b
    go run tools/main.go generate payment -t "6361887b-56cb-4de9-a013-aa600326a911" -s "97ee35cf-d288-4e24-8d03-6b0f7a583902" -c
