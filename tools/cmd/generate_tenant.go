package cmd

import (
	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

var generateTenantCmd = &cobra.Command{
	Use:   "tenant",
	Short: "A brief description",
	Long:  `A longer description.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Generated tenant json:")
		fmt.Println()

		endpoint_soap, err := cmd.Flags().GetString("endpoint_soap")
		if err != nil {
			panic(err)
		}
		endpoint_cart, err := cmd.Flags().GetString("endpoint_cart")
		if err != nil {
			panic(err)
		}
		production, err := cmd.Flags().GetBool("production")
		if err != nil {
			panic(err)
		}
		username, err := cmd.Flags().GetString("username")
		if err != nil {
			panic(err)
		}
		password, err := cmd.Flags().GetString("password")
		if err != nil {
			panic(err)
		}
		tax_number, err := cmd.Flags().GetString("tax_number")
		if err != nil {
			panic(err)
		}
		name, err := cmd.Flags().GetString("name")
		if err != nil {
			panic(err)
		}

		tenant := models.Tenant{
			ID:           uuid.NewV4().String(),
			Active:       true,
			EndpointSoap: endpoint_soap,
			EndpointCart: endpoint_cart,
			Production:   production,
			Username:     username,
			Password:     password,
			TaxNumber:    tax_number,
			Name:         name,
		}

		bytes, err := json.MarshalIndent(tenant, "", "  ")
		if err != nil {
			panic(err)
		}

		fmt.Println(string(bytes))
		fmt.Println()
	},
}

func init() {
	generateCmd.AddCommand(generateTenantCmd)

	generateTenantCmd.Flags().StringP("endpoint_soap", "e", "https://depag.e-pal.it/PagamentiOnLine/services/depag", "Endpoint soap del tenant")
	generateTenantCmd.Flags().StringP("endpoint_cart", "f", "https://api.uat.platform.pagopa.it/checkout/ec/v1", "Endpoint cart del tenant")
	generateTenantCmd.Flags().Bool("production", false, "Modalità production?")
	generateTenantCmd.Flags().StringP("username", "u", "OPENCONTENT", "Username del tenant")
	generateTenantCmd.Flags().StringP("password", "p", "OP3NC0NT3N7", "Password del tenant")
	generateTenantCmd.Flags().StringP("tax_number", "t", "00529840019", "Codice fiscale del tenant")
	generateTenantCmd.Flags().StringP("name", "n", "Comune di Rivoli", "Nome riferimento del tenant")
}
