package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

var generatePaymentCmd = &cobra.Command{
	Use:   "payment",
	Short: "A brief description",
	Long:  `A longer description.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Generated payment json:")
		fmt.Println()

		serverConfig, err := server.LoadConfig(context.Background())
		if err != nil {
			panic(err)
		}

		tenant_id, err := cmd.Flags().GetString("tenant_id")
		if err != nil {
			panic(err)
		}

		service_id, err := cmd.Flags().GetString("service_id")
		if err != nil {
			panic(err)
		}

		status, err := cmd.Flags().GetString("status")
		if err != nil {
			panic(err)
		}

		amount, err := cmd.Flags().GetFloat64("amount")
		if err != nil {
			panic(err)
		}

		modifiedSplit, err := cmd.Flags().GetBool("modified_split")
		if err != nil {
			panic(err)
		}

		exemptionSplit, err := cmd.Flags().GetBool("exemption_split")
		if err != nil {
			panic(err)
		}

		id := uuid.NewV4().String()
		remote_id := uuid.NewV4().String()

		now := models.TimeNow()
		expire := now.Add(365 * models.TimeDay)
		nextCheck := now.Add(1 * models.TimeHour)

		var payment models.Payment
		if status == "CREATION_PENDING" {
			c1amount := 14.00
			a1amount := 2.50
			amount = c1amount + a1amount
			paymentDetailSplit := []*models.PaymentDetailSplit{}
			if modifiedSplit {
				paymentDetailSplit = []*models.PaymentDetailSplit{
					{
						Code:   "c_1",
						Amount: &c1amount,
						Meta:   &models.PaymentDetailSplitMeta{},
					},
					{
						Code:   "a_1",
						Amount: &a1amount,
						Meta:   &models.PaymentDetailSplitMeta{},
					},
				}
			}
			if exemptionSplit {
				a1amount := 0.50
				amount = a1amount
				paymentDetailSplit = []*models.PaymentDetailSplit{
					{
						Code:   "c_1",
						Amount: nil,
						Meta:   &models.PaymentDetailSplitMeta{},
					},
					{
						Code:   "a_1",
						Amount: &a1amount,
						Meta:   &models.PaymentDetailSplitMeta{},
					},
				}
			}

			payment = models.Payment{
				ID:        id,
				UserID:    uuid.NewV4().String(),
				Type:      "PAGOPA",
				TenantID:  tenant_id,
				ServiceID: service_id,
				CreatedAt: now,
				UpdatedAt: now,
				Status:    status,
				Reason:    remote_id + " - BNRMHL75C06G702B",
				RemoteID:  remote_id,
				Payment: models.PaymentDetail{
					TransactionID: "",
					PaidAt:        models.Time{},
					ExpireAt:      expire,
					Amount:        amount,
					Currency:      "EUR",
					NoticeCode:    "",
					IUD:           strings.ReplaceAll(id, "-", ""),
					IUV:           "",
					Split:         paymentDetailSplit,
				},
				Links: models.PaymentLinks{
					OnlinePaymentBegin: models.PaymentLink{
						Url:          "",
						LastOpenedAt: models.Time{},
						Method:       "GET",
					},
					OnlinePaymentLanding: models.PaymentLink{
						Url:          "https://servizi.comune-qa.bugliano.pi.it/lang/it/pratiche/" + remote_id + "/detail",
						LastOpenedAt: models.Time{},
						Method:       "GET",
					},
					OfflinePayment: models.PaymentLink{
						Url:          "",
						LastOpenedAt: models.Time{},
						Method:       "GET",
					},
					Receipt: models.PaymentLink{
						Url:          "",
						LastOpenedAt: models.Time{},
						Method:       "GET",
					},
					Notify: []models.PaymentNotification{
						{
							Url:    "https://servizi.comune-qa.bugliano.pi.it/lang/api/applications/" + remote_id + "/payment",
							Method: "POST",
							SentAt: models.Time{},
						},
					},
					Update: models.PaymentUpdate{
						Url:         "",
						LastCheckAt: models.Time{},
						NextCheckAt: models.Time{},
						Method:      "GET",
					},
				},
				Payer: models.PaymentPayer{
					Type:                    "human",
					TaxIdentificationNumber: "BNRMHL75C06G702B",
					Name:                    "Michelangelo",
					FamilyName:              "Buonarroti",
					StreetName:              "Cesare Battisti",
					BuildingNumber:          "",
					PostalCode:              "38010",
					TownName:                "Bugliano",
					CountrySubdivision:      "PI",
					Country:                 "IT",
					Email:                   "office@zoonect.com",
				},
				EventID:        uuid.NewV4().String(),
				EventVersion:   "2.0",
				EventCreatedAt: now,
				AppID:          "payment-dispatcher:1.1.3",
			}
		} else {
			paymentDetailSplit := []*models.PaymentDetailSplit{}
			if modifiedSplit {
				c1amount := 14.00
				a1amount := 2.50
				amount = c1amount + a1amount
				paymentDetailSplit = []*models.PaymentDetailSplit{
					{
						Code:   "c_1",
						Amount: &c1amount,
						Meta: &models.PaymentDetailSplitMeta{
							Kind:      "9",
							EntryCode: "123",
							EntryAcc:  "123",
						},
					},
					{
						Code:   "a_1",
						Amount: &a1amount,
						Meta: &models.PaymentDetailSplitMeta{
							Kind:      "9",
							EntryCode: "123",
							EntryAcc:  "123",
						},
					},
				}
			}
			if exemptionSplit {
				a1amount := 0.50
				amount = a1amount
				paymentDetailSplit = []*models.PaymentDetailSplit{
					{
						Code:   "a_1",
						Amount: &a1amount,
						Meta: &models.PaymentDetailSplitMeta{
							Kind:      "9",
							EntryCode: "123",
							EntryAcc:  "123",
						},
					},
				}
			}

			payment = models.Payment{
				ID:        id,
				UserID:    uuid.NewV4().String(),
				Type:      "PAGOPA",
				TenantID:  tenant_id,
				ServiceID: service_id,
				CreatedAt: now,
				UpdatedAt: now,
				Status:    status,
				Reason:    remote_id + " - BNRMHL75C06G702B",
				RemoteID:  remote_id,
				Payment: models.PaymentDetail{
					TransactionID: "",
					PaidAt:        models.Time{},
					ExpireAt:      expire,
					Amount:        amount,
					Currency:      "EUR",
					NoticeCode:    "001550000000024427",
					IUD:           strings.ReplaceAll(id, "-", ""),
					IUV:           "550000000024427",
					Split:         paymentDetailSplit,
				},
				Links: models.PaymentLinks{
					OnlinePaymentBegin: models.PaymentLink{
						Url:          serverConfig.ExternalApiUrl + serverConfig.BasePath + "online-payment/" + id,
						LastOpenedAt: models.Time{},
						Method:       "GET",
					},
					OnlinePaymentLanding: models.PaymentLink{
						Url:          "https://servizi.comune-qa.bugliano.pi.it/lang/it/pratiche/" + remote_id + "/detail",
						LastOpenedAt: models.Time{},
						Method:       "GET",
					},
					OfflinePayment: models.PaymentLink{
						Url:          serverConfig.ExternalApiUrl + serverConfig.BasePath + "notice/" + id,
						LastOpenedAt: models.Time{},
						Method:       "GET",
					},
					Receipt: models.PaymentLink{
						Url:          serverConfig.ExternalApiUrl + serverConfig.BasePath + "receipt/" + id,
						LastOpenedAt: models.Time{},
						Method:       "GET",
					},
					Notify: []models.PaymentNotification{
						{
							Url:    "https://servizi.comune-qa.bugliano.pi.it/lang/api/applications/" + remote_id + "/payment",
							Method: "POST",
							SentAt: models.Time{},
						},
					},
					Update: models.PaymentUpdate{
						Url:         serverConfig.InternalApiUrl + serverConfig.BasePath + "update/" + id,
						LastCheckAt: now,
						NextCheckAt: nextCheck,
						Method:      "GET",
					},
				},
				Payer: models.PaymentPayer{
					Type:                    "human",
					TaxIdentificationNumber: "BNRMHL75C06G702B",
					Name:                    "Michelangelo",
					FamilyName:              "Buonarroti",
					StreetName:              "Cesare Battisti",
					BuildingNumber:          "",
					PostalCode:              "38010",
					TownName:                "Bugliano",
					CountrySubdivision:      "PI",
					Country:                 "IT",
					Email:                   "office@zoonect.com",
				},
				EventID:        uuid.NewV4().String(),
				EventVersion:   "2.0",
				EventCreatedAt: now,
				AppID:          serverConfig.AppName + ":" + server.VERSION,
			}
		}

		bytes, err := json.MarshalIndent(payment, "", "  ")
		if err != nil {
			panic(err)
		}

		fmt.Println(string(bytes))
		fmt.Println()
	},
}

func init() {
	generateCmd.AddCommand(generatePaymentCmd)

	generatePaymentCmd.Flags().StringP("tenant_id", "t", "", "ID del tenant")
	generatePaymentCmd.MarkFlagRequired("tenant_id")
	generatePaymentCmd.Flags().StringP("service_id", "s", "", "ID del service")
	generatePaymentCmd.MarkFlagRequired("service_id")
	generatePaymentCmd.Flags().StringP("status", "u", "CREATION_PENDING", "Stato del pagamento")
	generatePaymentCmd.Flags().Float64P("amount", "a", 1234.56, "Importo del pagamento")
	generatePaymentCmd.Flags().BoolP("modified_split", "b", false, "Con bilancio modificato")
	generatePaymentCmd.Flags().BoolP("exemption_split", "c", false, "Con bilancio esenzione")
}
