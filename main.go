package main

import (
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server"
)

func main() {
	serverContext := server.InitServerContext()

	serverContext.StartProcess("kafka server", server.StartKafkaServer)
	serverContext.StartProcess("http server", server.StartHttpServer)

	serverContext.Daemonize()
}
