package server

import (
	"encoding/json"
	"strings"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

type FlowEventPaymentReceived struct {
	Name          string
	Sctx          *ServerContext
	Event         *kafka.Message
	Tenant        *models.Tenant
	Service       *models.Service
	StoredPayment *models.Payment
	Err           error
	Msg           string
	Status        bool
	Result        *models.Payment
}

func (flow *FlowEventPaymentReceived) Exec() bool {
	flow.Name = "EventPaymentReceived"

	flow.Status = true &&
		flow.extractPayment() &&
		flow.loadRelatedTenant() &&
		flow.loadRelatedService() &&
		flow.checkPrecondition() &&
		flow.loadStoredPayment() &&
		flow.checkPostcondition()

	if flow.Status {
		MetricsPaymentsReceived.Inc()
	}

	return flow.Status
}

func (flow *FlowEventPaymentReceived) extractPayment() bool {
	eventValue := flow.Event.Value

	flow.Result = &models.Payment{}
	err := json.Unmarshal(eventValue, flow.Result)
	if err != nil {
		flow.Err = err
		flow.Msg = "json unmarshal error"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if flow.Result.EventVersion != "2.0" {
		flow.Err = err
		flow.Msg = "unsupported event version"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if flow.Result.TenantID == "" || flow.Result.ServiceID == "" {
		flow.Err = err
		flow.Msg = "missing tenant_id or service_id"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventPaymentReceived) loadRelatedTenant() bool {
	tenantsCache := flow.Sctx.TenantsCache()

	tenant, err := tenantsCache.Get(flow.Sctx.Ctx(), flow.Result.TenantID)
	flow.Tenant = tenant
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant tenant"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get tenant by id error for: " + flow.Result.TenantID

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventPaymentReceived) loadRelatedService() bool {
	servicesCache := flow.Sctx.ServicesCache()

	service, err := servicesCache.Get(flow.Sctx.Ctx(), flow.Result.ServiceID)
	flow.Service = service
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant service"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get service by id error for: " + flow.Result.ServiceID

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventPaymentReceived) checkPrecondition() bool {
	if flow.Result.TenantID != flow.Service.TenantID {
		flow.Msg = "wrong tenant for this service"
		return false
	}
	if !flow.Tenant.Active {
		flow.Msg = "tenant not active"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if !flow.Service.Active {
		flow.Msg = "service not active"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	if flow.Result.ID == "" {
		flow.Msg = "missing id"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventPaymentReceived) loadStoredPayment() bool {
	paymentsCache := flow.Sctx.PaymentsCache()

	storedPayment, err := paymentsCache.Get(flow.Sctx.Ctx(), flow.Result.ID)
	flow.StoredPayment = storedPayment
	if err != nil && (err.Error() == "value not found in store" || err.Error() == "not found") {
		flow.Msg = "new payment found"

		MetricsPaymentsNew.Inc()

		return true
	}
	if err != nil && err.Error() != "value not found in store" && err.Error() != "not found" {
		flow.Msg = "get payment by id error for: " + flow.Result.ID

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventPaymentReceived) checkPostcondition() bool {
	serverConfig := flow.Sctx.ServerConfig()

	if flow.StoredPayment == nil && flow.Result.Status != "CREATION_PENDING" {
		flow.Msg = "payment not in CREATION_PENDING status should be found on storage"

		MetricsPaymentsValidationError.Inc()

		return false
	}
	if flow.StoredPayment != nil && flow.Result.ID == flow.StoredPayment.ID {
		flow.Msg = "payment already handled"

		return false
	}
	if strings.HasPrefix(flow.Result.AppID, serverConfig.AppName) {
		flow.Msg = "payment event is from this proxy"
		return false
	}

	return true
}
