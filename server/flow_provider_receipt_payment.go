package server

import (
	"crypto/tls"
	"encoding/base64"
	"encoding/xml"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models/depag"
)

type FlowProviderReceiptPayment struct {
	Name     string
	Sctx     *ServerContext
	Payment  *models.Payment
	Service  *models.Service
	Tenant   *models.Tenant
	Request  *depag.LeggiRicevutaTelematica
	Client   *soap.Client
	Depag    depag.Service
	Err      error
	Msg      string
	Status   bool
	Result   *depag.LeggiRicevutaTelematicaResponse
	Received bool
	Receipt  []byte
}

func (flow *FlowProviderReceiptPayment) Exec() bool {
	flow.Name = "ProviderReceiptPayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderReceiptPayment) prepareRequest() bool {
	param := &models.DepagReceiptRequest{
		Iuv: flow.Payment.Payment.IUV,
	}

	xmlParam, err := xml.Marshal(param)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while encoding param"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	encodedParam := base64.StdEncoding.EncodeToString(xmlParam)

	flow.Request = &depag.LeggiRicevutaTelematica{
		Ente:  flow.Tenant.TaxNumber,
		Param: encodedParam,
	}

	return true
}

func (flow *FlowProviderReceiptPayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_receipt"))

	flow.Client = soap.NewClient(flow.Tenant.EndpointSoap, soap.WithBasicAuth(flow.Tenant.Username, flow.Tenant.Password), soap.WithTLS(&tls.Config{Renegotiation: tls.RenegotiateOnceAsClient}))

	flow.Depag = depag.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Depag.LeggiRicevutaTelematica(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting notice request"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	encodedParam := flow.Result.Dovuti.Param
	if encodedParam == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	xmlParam, err := base64.StdEncoding.DecodeString(encodedParam)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding param"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	param := &models.DepagReceiptResponse{}
	err = xml.Unmarshal(xmlParam, param)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while unmarshalling param"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	if param.Result != "OK" || param.Data == nil {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	receipt := param.Data.Receipt
	if receipt == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Receipt, err = base64.StdEncoding.DecodeString(receipt)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding notice"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	flow.Received = true

	return true
}
