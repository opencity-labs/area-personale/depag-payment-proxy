package server

import (
	"context"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
)

type healthCheck struct {
	Detail   string `json:"detail" description:"Error detail."`
	Type     string `json:"type" description:"Error type."`
	Status   int    `json:"status" description:"Error status." format:"int32"`
	Title    string `json:"title" description:"Error title."`
	Instance string `json:"instance" description:"Error instance."`
}

func GetStatus(sctx *ServerContext) usecase.Interactor {
	basePath := sctx.ServerConfig().BasePath

	uc := usecase.NewInteractor(func(ctx context.Context, _ struct{}, output *healthCheck) error {
		*output = healthCheck{
			Detail:   "Checked at: " + time.Now().String() + ", uuid: " + uuid.NewV4().String(),
			Type:     "https://httpstatuses.io/200",
			Status:   200,
			Title:    "OK",
			Instance: basePath + "status",
		}

		return nil
	})

	uc.SetTitle("Status health-check")
	uc.SetDescription("...")
	uc.SetTags("Utilities")
	uc.SetExpectedErrors(status.Internal)

	return uc
}
