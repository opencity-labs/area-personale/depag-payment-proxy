package server

import (
	"context"
	"encoding/binary"
	"strings"

	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
)

type getPaymentOnlineInput struct {
	PaymentID string `path:"payment_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

type getPaymentOnlineOutput struct {
	Location string `header:"Location"`
}

func GetPaymentOnline(sctx *ServerContext) usecase.Interactor {
	tenantsSync := sctx.TenantsSync()
	servicesSync := sctx.ServicesSync()
	paymentsSync := sctx.PaymentsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getPaymentOnlineInput, output *getPaymentOnlineOutput) error {
		tenantsSync.RLock()
		defer tenantsSync.RUnlock()
		servicesSync.RLock()
		defer servicesSync.RUnlock()
		paymentsSync.Lock()
		defer paymentsSync.Unlock()

		flowRequestPaymentOnline := &FlowRequestPaymentOnline{
			Sctx:      sctx,
			PaymentID: input.PaymentID,
		}
		if !flowRequestPaymentOnline.Exec() {
			if flowRequestPaymentOnline.Err != nil {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentOnline.Name).
					Stack().Err(flowRequestPaymentOnline.Err).
					Msg(flowRequestPaymentOnline.Msg)
			} else {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentOnline.Name).
					Msg(flowRequestPaymentOnline.Msg)
			}

			if flowRequestPaymentOnline.Msg == "payment not found" {
				return status.NotFound
			}
			if strings.HasPrefix(flowRequestPaymentOnline.Msg, "irrilevant payment") {
				return status.InvalidArgument
			}
			return status.Internal
		}

		output.Location = flowRequestPaymentOnline.Url

		return nil
	})

	uc.SetTitle("Start online payment flow")
	uc.SetDescription("...")
	uc.SetTags("Payments")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type getPaymentLandingInput struct {
	PaymentID string `path:"payment_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Result    string `query:"esito"` // ok o ko
	IUV       string `json:"iuv"`
	Message   string `json:"messaggio"`
}

type getPaymentLandingOutput struct {
	Location string `header:"Location"`
}

func GetPaymentLanding(sctx *ServerContext) usecase.Interactor {
	tenantsSync := sctx.TenantsSync()
	servicesSync := sctx.ServicesSync()
	paymentsSync := sctx.PaymentsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getPaymentLandingInput, output *getPaymentLandingOutput) error {
		tenantsSync.RLock()
		defer tenantsSync.RUnlock()
		servicesSync.RLock()
		defer servicesSync.RUnlock()
		paymentsSync.Lock()
		defer paymentsSync.Unlock()

		flowRequestPaymentLanding := &FlowRequestPaymentLanding{
			Sctx:          sctx,
			PaymentID:     input.PaymentID,
			PaymentResult: input.Result == "ok",
		}
		if !flowRequestPaymentLanding.Exec() {
			if flowRequestPaymentLanding.Err != nil {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentLanding.Name).
					Stack().Err(flowRequestPaymentLanding.Err).
					Msg(flowRequestPaymentLanding.Msg)
			} else {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentLanding.Name).
					Msg(flowRequestPaymentLanding.Msg)
			}

			if flowRequestPaymentLanding.Msg == "payment not found" {
				return status.NotFound
			}
			if strings.HasPrefix(flowRequestPaymentLanding.Msg, "irrilevant payment") {
				return status.InvalidArgument
			}
			return status.Internal
		}

		output.Location = flowRequestPaymentLanding.Url

		return nil
	})

	uc.SetTitle("Landing from online payment flow")
	uc.SetDescription("...")
	uc.SetTags("Payments")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type getPaymentNoticeInput struct {
	PaymentID string `path:"payment_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

type getPaymentNoticeOutput struct {
	ContentDisposition string `header:"Content-Disposition" description:"..."`
	ContentLength      int    `header:"Content-Length" description:"..." format:"int64"`
	CacheControl       string `header:"Cache-Control" description:"no-cache ..."`
	usecase.OutputWithEmbeddedWriter
}

func GetPaymentNotice(sctx *ServerContext) usecase.Interactor {
	tenantsSync := sctx.TenantsSync()
	servicesSync := sctx.ServicesSync()
	paymentsSync := sctx.PaymentsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getPaymentNoticeInput, output *getPaymentNoticeOutput) error {
		tenantsSync.RLock()
		defer tenantsSync.RUnlock()
		servicesSync.RLock()
		defer servicesSync.RUnlock()
		paymentsSync.Lock()
		defer paymentsSync.Unlock()

		flowRequestPaymentNotice := &FlowRequestPaymentNotice{
			Sctx:      sctx,
			PaymentID: input.PaymentID,
		}
		if !flowRequestPaymentNotice.Exec() {
			if flowRequestPaymentNotice.Err != nil {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentNotice.Name).
					Stack().Err(flowRequestPaymentNotice.Err).
					Msg(flowRequestPaymentNotice.Msg)
			} else {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentNotice.Name).
					Msg(flowRequestPaymentNotice.Msg)
			}

			if flowRequestPaymentNotice.Msg == "payment not found" {
				return status.NotFound
			}
			if strings.HasPrefix(flowRequestPaymentNotice.Msg, "irrilevant payment") {
				return status.InvalidArgument
			}
			return status.Internal
		}

		output.ContentDisposition = "attachment; filename=\"avviso-" + flowRequestPaymentNotice.Result.Payment.IUD + ".pdf\""
		output.ContentLength = len(flowRequestPaymentNotice.Notice)
		output.CacheControl = "no-cache"

		return binary.Write(output, binary.BigEndian, flowRequestPaymentNotice.Notice)
	})

	uc.SetTitle("Get offline payment notice")
	uc.SetDescription("...")
	uc.SetTags("Payments")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type getPaymentReceiptInput struct {
	PaymentID string `path:"payment_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

type getPaymentReceiptOutput struct {
	ContentDisposition string `header:"Content-Disposition" description:"..."`
	ContentLength      int    `header:"Content-Length" description:"..." format:"int64"`
	CacheControl       string `header:"Cache-Control" description:"no-cache ..."`
	usecase.OutputWithEmbeddedWriter
}

func GetPaymentReceipt(sctx *ServerContext) usecase.Interactor {
	tenantsSync := sctx.TenantsSync()
	servicesSync := sctx.ServicesSync()
	paymentsSync := sctx.PaymentsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getPaymentReceiptInput, output *getPaymentReceiptOutput) error {
		tenantsSync.RLock()
		defer tenantsSync.RUnlock()
		servicesSync.RLock()
		defer servicesSync.RUnlock()
		paymentsSync.Lock()
		defer paymentsSync.Unlock()

		flowRequestPaymentReceipt := &FlowRequestPaymentReceipt{
			Sctx:      sctx,
			PaymentID: input.PaymentID,
		}
		if !flowRequestPaymentReceipt.Exec() {
			if flowRequestPaymentReceipt.Err != nil {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentReceipt.Name).
					Stack().Err(flowRequestPaymentReceipt.Err).
					Msg(flowRequestPaymentReceipt.Msg)
			} else {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentReceipt.Name).
					Msg(flowRequestPaymentReceipt.Msg)
			}

			if flowRequestPaymentReceipt.Msg == "payment not found" {
				return status.NotFound
			}
			if strings.HasPrefix(flowRequestPaymentReceipt.Msg, "irrilevant payment") {
				return status.InvalidArgument
			}
			return status.Internal
		}

		if !flowRequestPaymentReceipt.FlowProviderReceiptPayment.Received {
			return status.InvalidArgument
		}

		output.ContentDisposition = "attachment; filename=\"ricevuta-" + flowRequestPaymentReceipt.Result.Payment.IUD + ".pdf\""
		output.ContentLength = len(flowRequestPaymentReceipt.Receipt)
		output.CacheControl = "no-cache"

		return binary.Write(output, binary.BigEndian, flowRequestPaymentReceipt.Receipt)
	})

	uc.SetTitle("Get payment receipt")
	uc.SetDescription("...")
	uc.SetTags("Payments")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type getPaymentUpdateInput struct {
	PaymentID string `path:"payment_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetPaymentUpdate(sctx *ServerContext) usecase.Interactor {
	tenantsSync := sctx.TenantsSync()
	servicesSync := sctx.ServicesSync()
	paymentsSync := sctx.PaymentsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getPaymentUpdateInput, _ *struct{}) error {
		tenantsSync.RLock()
		defer tenantsSync.RUnlock()
		servicesSync.RLock()
		defer servicesSync.RUnlock()
		paymentsSync.Lock()
		defer paymentsSync.Unlock()

		flowRequestPaymentUpdate := &FlowRequestPaymentUpdate{
			Sctx:      sctx,
			PaymentID: input.PaymentID,
		}
		if !flowRequestPaymentUpdate.Exec() {
			if flowRequestPaymentUpdate.Err != nil {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentUpdate.Name).
					Stack().Err(flowRequestPaymentUpdate.Err).
					Msg(flowRequestPaymentUpdate.Msg)
			} else {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentUpdate.Name).
					Msg(flowRequestPaymentUpdate.Msg)
			}

			if flowRequestPaymentUpdate.Msg == "payment not found" {
				return status.NotFound
			}
			if strings.HasPrefix(flowRequestPaymentUpdate.Msg, "irrilevant payment") {
				return status.InvalidArgument
			}
			return status.Internal
		}

		return nil
	})

	uc.SetTitle("Check payment status")
	uc.SetDescription("...")
	uc.SetTags("Payments")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type getPaymentNotifyInput struct {
	PaymentID string `form:"CHIAVE" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	PayedAt   string `form:"DATA_PAGAMENTO" description:"..." example:"15/12/2023 00.00.00"`
	Iuv       string `form:"IUV" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetPaymentNotify(sctx *ServerContext) usecase.Interactor {
	tenantsSync := sctx.TenantsSync()
	servicesSync := sctx.ServicesSync()
	paymentsSync := sctx.PaymentsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getPaymentNotifyInput, output *string) error {
		tenantsSync.RLock()
		defer tenantsSync.RUnlock()
		servicesSync.RLock()
		defer servicesSync.RUnlock()
		paymentsSync.Lock()
		defer paymentsSync.Unlock()

		sctx.LogHttpInfo().
			Str("method", "POST").
			Str("path", sctx.ServerConfig().BasePath+"notify-payment").
			Interface("input", input).
			Msg("debug-notify-0")

		flowRequestPaymentNotify := &FlowRequestPaymentNotify{
			Sctx:      sctx,
			PaymentID: input.PaymentID,
			PayedAt:   input.PayedAt,
			Iuv:       input.Iuv,
		}
		if !flowRequestPaymentNotify.Exec() {
			if flowRequestPaymentNotify.Err != nil {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentNotify.Name).
					Stack().Err(flowRequestPaymentNotify.Err).
					Msg(flowRequestPaymentNotify.Msg)
			} else {
				sctx.LogHttpDebug().Str("result", "discarded").
					Str("flow", flowRequestPaymentNotify.Name).
					Msg(flowRequestPaymentNotify.Msg)
			}

			if flowRequestPaymentNotify.Msg == "payment not found" {
				return status.NotFound
			}
			if strings.HasPrefix(flowRequestPaymentNotify.Msg, "irrilevant payment") {
				return status.InvalidArgument
			}
			return status.Internal
		}

		*output = "1"

		return nil
	})

	uc.SetTitle("Notify payment update")
	uc.SetDescription("...")
	uc.SetTags("Payments")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}
