package server

import (
	"context"

	uuid "github.com/satori/go.uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

func GetServiceSchema(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, _ struct{}, output *models.Schema) error {
		*output = models.ServiceSchema

		return nil
	})

	uc.SetTitle("Get Service Form Schema")
	uc.SetDescription("...")
	uc.SetTags("Services")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

type getServiceByIDInput struct {
	ServiceId string `path:"service_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetServiceByID(sctx *ServerContext) usecase.Interactor {
	servicesCache := sctx.ServicesCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getServiceByIDInput, output *models.Service) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		service, err := servicesCache.Get(ctx, input.ServiceId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get service by id error for: " + input.ServiceId)
			return status.Internal
		}

		*output = *service

		return nil
	})

	uc.SetTitle("Get Service Configuration")
	uc.SetDescription("...")
	uc.SetTags("Services")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type createServiceInput struct {
	ID          string                            `json:"id" description:"..." required:"true"`
	TenantID    string                            `json:"tenant_id" description:"..." required:"true"`
	Active      bool                              `json:"active" description:"..." required:"true"`
	Code        string                            `json:"code" description:"..." required:"true"`
	Description string                            `json:"description" description:"..." required:"true"`
	Splitted    bool                              `json:"splitted" description:"..." required:"true"`
	Split       []*createServiceInputServiceSplit `json:"split" description:"..." required:"true"`
}

type createServiceInputServiceSplit struct {
	Code      string  `json:"split_code" description:"..." required:"true"`
	Amount    float64 `json:"split_amount" description:"..." required:"true" format:"float"`
	Kind      string  `json:"split_kind" description:"..." required:"true"`
	EntryCode string  `json:"split_entry_code" description:"..." required:"true"`
	EntryAcc  string  `json:"split_entry_acc" description:"..." required:"true"`
}

func CreateService(sctx *ServerContext) usecase.Interactor {
	servicesCache := sctx.ServicesCache()
	tenantsCache := sctx.TenantsCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input createServiceInput, output *models.Service) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		_, err := servicesCache.Get(ctx, input.ID)

		if err == nil || err.Error() != "value not found in store" {
			return status.AlreadyExists
		}

		_, err = tenantsCache.Get(ctx, input.TenantID)
		if err != nil && err.Error() == "value not found in store" {
			return status.InvalidArgument
		}

		_, err = uuid.FromString(input.ID)
		if err != nil {
			return status.InvalidArgument
		}

		_, err = uuid.FromString(input.TenantID)
		if err != nil {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.TenantID = input.TenantID
		output.Active = input.Active
		output.Code = input.Code
		output.Description = input.Description
		output.Splitted = input.Splitted
		output.Split = []models.ServiceSplit{}
		if input.Split != nil {
			for _, split := range input.Split {
				outputSplit := models.ServiceSplit{
					Code:      split.Code,
					Amount:    split.Amount,
					Kind:      split.Kind,
					EntryCode: split.EntryCode,
					EntryAcc:  split.EntryAcc,
				}
				output.Split = append(output.Split, outputSplit)
			}
		}

		err = StoreService(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Save Service Configuration")
	uc.SetDescription("...")
	uc.SetTags("Services")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type updateServiceInput struct {
	ServiceId   string                            `path:"service_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ID          string                            `json:"id" description:"..." required:"true"`
	TenantID    string                            `json:"tenant_id" description:"..." required:"true"`
	Active      bool                              `json:"active" description:"..." required:"true"`
	Code        string                            `json:"code" description:"..." required:"true"`
	Description string                            `json:"description" description:"..." required:"true"`
	Splitted    bool                              `json:"splitted" description:"..." required:"true"`
	Split       []*updateServiceInputServiceSplit `json:"split" description:"..." required:"true"`
}

type updateServiceInputServiceSplit struct {
	Code      string  `json:"split_code" description:"..." required:"true"`
	Amount    float64 `json:"split_amount" description:"..." required:"true" format:"float"`
	Kind      string  `json:"split_kind" description:"..." required:"true"`
	EntryCode string  `json:"split_entry_code" description:"..." required:"true"`
	EntryAcc  string  `json:"split_entry_acc" description:"..." required:"true"`
}

func UpdateService(sctx *ServerContext) usecase.Interactor {
	servicesCache := sctx.ServicesCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input updateServiceInput, output *models.Service) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		service, err := servicesCache.Get(ctx, input.ServiceId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get service by id error for: " + input.ServiceId)
			return status.Internal
		}

		if input.ID != service.ID || input.TenantID != service.TenantID {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.TenantID = input.TenantID
		output.Active = input.Active
		output.Code = input.Code
		output.Description = input.Description
		output.Splitted = input.Splitted
		output.Split = []models.ServiceSplit{}
		if input.Split != nil {
			for _, split := range input.Split {
				outputSplit := models.ServiceSplit{
					Code:      split.Code,
					Amount:    split.Amount,
					Kind:      split.Kind,
					EntryCode: split.EntryCode,
					EntryAcc:  split.EntryAcc,
				}
				output.Split = append(output.Split, outputSplit)
			}
		}

		err = StoreService(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Service Configuration")
	uc.SetDescription("...")
	uc.SetTags("Services")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type patchServiceInput struct {
	ServiceId   string                           `path:"service_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Active      *bool                            `json:"active" description:"..."`
	Code        *string                          `json:"code" description:"..."`
	Description *string                          `json:"description" description:"..."`
	Splitted    *bool                            `json:"splitted" description:"..."`
	Split       []*patchServiceInputServiceSplit `json:"split" description:"..."`
}

type patchServiceInputServiceSplit struct {
	Code      string  `json:"split_code" description:"..."`
	Amount    float64 `json:"split_amount" description:"..." format:"float"`
	Kind      string  `json:"split_kind" description:"..."`
	EntryCode string  `json:"split_entry_code" description:"..."`
	EntryAcc  string  `json:"split_entry_acc" description:"..."`
}

func PatchService(sctx *ServerContext) usecase.Interactor {
	servicesCache := sctx.ServicesCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input patchServiceInput, output *models.Service) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		service, err := servicesCache.Get(ctx, input.ServiceId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get service by id error for: " + input.ServiceId)
			return status.Internal
		}

		output.ID = service.ID
		output.TenantID = service.TenantID

		if input.Active == nil {
			output.Active = service.Active
		} else {
			output.Active = *input.Active
		}
		if input.Code == nil {
			output.Code = service.Code
		} else {
			output.Code = *input.Code
		}
		if input.Description == nil {
			output.Description = service.Description
		} else {
			output.Description = *input.Description
		}
		if input.Splitted == nil {
			output.Splitted = service.Splitted
		} else {
			output.Splitted = *input.Splitted
		}
		if input.Split == nil {
			output.Split = service.Split
		} else {
			output.Split = []models.ServiceSplit{}
			if input.Split != nil {
				for _, split := range input.Split {
					outputSplit := models.ServiceSplit{
						Code:      split.Code,
						Amount:    split.Amount,
						Kind:      split.Kind,
						EntryCode: split.EntryCode,
						EntryAcc:  split.EntryAcc,
					}
					output.Split = append(output.Split, outputSplit)
				}
			}
		}

		err = StoreService(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Existing Service Configuration")
	uc.SetDescription("...")
	uc.SetTags("Services")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type disableServiceInput struct {
	ServiceId string `path:"service_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func DisableService(sctx *ServerContext) usecase.Interactor {
	servicesCache := sctx.ServicesCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input disableServiceInput, _ *struct{}) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		service, err := servicesCache.Get(ctx, input.ServiceId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get service by id error for: " + input.ServiceId)
			return status.Internal
		}

		service.Active = false

		err = StoreService(sctx, service)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Disable Service Configuration")
	uc.SetDescription("...")
	uc.SetTags("Services")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}
