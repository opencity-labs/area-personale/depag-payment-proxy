package server

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

type FlowRequestPaymentNotify struct {
	Name      string
	Sctx      *ServerContext
	PaymentID string
	PayedAt   string
	Iuv       string
	Tenant    *models.Tenant
	Service   *models.Service
	Err       error
	Msg       string
	Status    bool
	Result    *models.Payment
}

func (flow *FlowRequestPaymentNotify) Exec() bool {
	flow.Name = "RequestPaymentUpdate"

	flow.Status = true &&
		flow.findPayment() &&
		flow.findTenant() &&
		flow.findService() &&
		flow.checkPrecondition() &&
		flow.updatePayment()

	return flow.Status
}

func (flow *FlowRequestPaymentNotify) findPayment() bool {
	paymentsCache := flow.Sctx.PaymentsCache()

	payment, err := paymentsCache.Get(flow.Sctx.Ctx(), flow.PaymentID)
	flow.Result = payment
	if err != nil && (err.Error() == "value not found in store" || err.Error() == "not found") {
		flow.Sctx.LogHttpInfo().
			Str("method", "POST").
			Str("path", flow.Sctx.ServerConfig().BasePath+"notify-payment").
			Str("paymentId", flow.PaymentID).
			Str("payedAt", flow.PayedAt).
			Str("iuv", flow.Iuv).
			Msg("debug-notify-1-payment-not-found")

		flow.Err = err
		flow.Msg = "payment not found"
		return false
	}
	if err != nil {
		flow.Sctx.LogHttpInfo().
			Str("method", "POST").
			Str("path", flow.Sctx.ServerConfig().BasePath+"notify-payment").
			Str("paymentId", flow.PaymentID).
			Str("payedAt", flow.PayedAt).
			Str("iuv", flow.Iuv).
			Msg("debug-notify-2-get-payment-by-id-error")

		flow.Err = err
		flow.Msg = "get payment by id error for: " + flow.PaymentID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentNotify) findTenant() bool {
	tenantsCache := flow.Sctx.TenantsCache()

	tenant, err := tenantsCache.Get(flow.Sctx.Ctx(), flow.Result.TenantID)
	flow.Tenant = tenant
	if err != nil && err.Error() == "value not found in store" {
		flow.Sctx.LogHttpInfo().
			Str("method", "POST").
			Str("path", flow.Sctx.ServerConfig().BasePath+"notify-payment").
			Str("paymentId", flow.PaymentID).
			Str("payedAt", flow.PayedAt).
			Str("iuv", flow.Iuv).
			Msg("debug-notify-3-tenant-not-found")

		flow.Err = err
		flow.Msg = "irrilevant payment, tenant not found"
		return false
	}
	if err != nil {
		flow.Sctx.LogHttpInfo().
			Str("method", "POST").
			Str("path", flow.Sctx.ServerConfig().BasePath+"notify-payment").
			Str("paymentId", flow.PaymentID).
			Str("payedAt", flow.PayedAt).
			Str("iuv", flow.Iuv).
			Msg("debug-notify-4-get-tenant-by-id-error")

		flow.Err = err
		flow.Msg = "get tenant by id error for: " + flow.Result.TenantID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentNotify) findService() bool {
	servicesCache := flow.Sctx.ServicesCache()

	service, err := servicesCache.Get(flow.Sctx.Ctx(), flow.Result.ServiceID)
	flow.Service = service
	if err != nil && err.Error() == "value not found in store" {
		flow.Sctx.LogHttpInfo().
			Str("method", "POST").
			Str("path", flow.Sctx.ServerConfig().BasePath+"notify-payment").
			Str("paymentId", flow.PaymentID).
			Str("payedAt", flow.PayedAt).
			Str("iuv", flow.Iuv).
			Msg("debug-notify-5-service-not-found")

		flow.Err = err
		flow.Msg = "irrilevant payment, service not found"
		return false
	}
	if err != nil {
		flow.Sctx.LogHttpInfo().
			Str("method", "POST").
			Str("path", flow.Sctx.ServerConfig().BasePath+"notify-payment").
			Str("paymentId", flow.PaymentID).
			Str("payedAt", flow.PayedAt).
			Str("iuv", flow.Iuv).
			Msg("debug-notify-6-get-service-by-id-error")

		flow.Err = err
		flow.Msg = "get service by id error for: " + flow.Result.ServiceID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentNotify) checkPrecondition() bool {
	if !flow.Tenant.Active {
		flow.Sctx.LogHttpInfo().
			Str("method", "POST").
			Str("path", flow.Sctx.ServerConfig().BasePath+"notify-payment").
			Str("paymentId", flow.PaymentID).
			Str("payedAt", flow.PayedAt).
			Str("iuv", flow.Iuv).
			Msg("debug-notify-7-inactive-tenant")

		flow.Msg = "irrilevant payment, tenant not active"
		return false
	}

	if !flow.Service.Active {
		flow.Sctx.LogHttpInfo().
			Str("method", "POST").
			Str("path", flow.Sctx.ServerConfig().BasePath+"notify-payment").
			Str("paymentId", flow.PaymentID).
			Str("payedAt", flow.PayedAt).
			Str("iuv", flow.Iuv).
			Msg("debug-notify-8-inactive-service")

		flow.Msg = "irrilevant payment, service not active"
		return false
	}

	if flow.Result.Status != "PAYMENT_PENDING" && flow.Result.Status != "PAYMENT_STARTED" {
		flow.Sctx.LogHttpInfo().
			Str("method", "POST").
			Str("path", flow.Sctx.ServerConfig().BasePath+"notify-payment").
			Str("paymentId", flow.PaymentID).
			Str("payedAt", flow.PayedAt).
			Str("iuv", flow.Iuv).
			Msg("debug-notify-9-wrong-payment-status-" + flow.Result.Status)

		flow.Msg = "irrilevant payment, current status not valid for this action"
		return false
	}

	if flow.Result.Payment.IUV != flow.Iuv {
		flow.Sctx.LogHttpInfo().
			Str("method", "POST").
			Str("path", flow.Sctx.ServerConfig().BasePath+"notify-payment").
			Str("paymentId", flow.PaymentID).
			Str("payedAt", flow.PayedAt).
			Str("iuv", flow.Iuv).
			Msg("debug-notify-10-payment-iuv-not-matching-" + flow.Result.Payment.IUV)

		flow.Msg = "payment iuv not matching"
		return false
	}

	return true
}

func (flow *FlowRequestPaymentNotify) updatePayment() bool {
	serverConfig := flow.Sctx.ServerConfig()

	now := models.TimeNow()
	flow.Result.UpdatedAt = now
	flow.Result.EventID = uuid.NewV4().String()
	flow.Result.EventCreatedAt = now
	flow.Result.EventVersion = "2.0"
	flow.Result.AppID = serverConfig.AppName + ":" + VERSION
	flow.Result.Links.Update.LastCheckAt = now

	payedAt, err := time.Parse("02/01/2006 15.04.05", flow.PayedAt)
	if flow.PayedAt != "" && err == nil {
		flow.Result.Status = "COMPLETE"
		flow.Result.Payment.PaidAt = models.Time{Time: payedAt}
	} else {
		flow.Result.Links.Update.NextCheckAt = flow.Result.NextCheck()
	}

	err = StorePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "storage error"
		return false
	}

	err = ProducePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "kafka producer error"
		return false
	}

	return true
}
