package server

import (
	"context"

	uuid "github.com/satori/go.uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

func GetTenantSchema(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, _ struct{}, output *models.Schema) error {
		*output = models.TenantSchema

		return nil
	})

	uc.SetTitle("Get Tenant Form Schema")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

type getTenantByIDInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetTenantByID(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getTenantByIDInput, output *models.Tenant) error {
		tenantsSync.RLock()
		defer tenantsSync.RUnlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		*output = *tenant

		return nil
	})

	uc.SetTitle("Get Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type createTenantInput struct {
	ID           string `json:"id" description:"..." required:"true"`
	Active       bool   `json:"active" description:"..." required:"true"`
	EndpointSoap string `json:"endpoint_soap" description:"..." required:"true"`
	EndpointCart string `json:"endpoint_cart" description:"..." required:"true"`
	Production   bool   `json:"production" description:"..." required:"true"`
	Username     string `json:"username" description:"..." required:"true"`
	Password     string `json:"password" description:"..." required:"true"`
	TaxNumber    string `json:"tax_number" description:"..." required:"true"`
	Name         string `json:"name" description:"..." required:"true"`
}

func CreateTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input createTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		_, err := tenantsCache.Get(ctx, input.ID)

		if err == nil || err.Error() != "value not found in store" {
			return status.AlreadyExists
		}

		_, err = uuid.FromString(input.ID)
		if err != nil {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.Active = input.Active
		output.EndpointSoap = input.EndpointSoap
		output.EndpointCart = input.EndpointCart
		output.Production = input.Production
		output.Username = input.Username
		output.Password = input.Password
		output.TaxNumber = input.TaxNumber
		output.Name = input.Name

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Save Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type updateTenantInput struct {
	TenantId     string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ID           string `json:"id" description:"..." required:"true"`
	Active       bool   `json:"active" description:"..." required:"true"`
	EndpointSoap string `json:"endpoint_soap" description:"..." required:"true"`
	EndpointCart string `json:"endpoint_cart" description:"..." required:"true"`
	Production   bool   `json:"production" description:"..." required:"true"`
	Username     string `json:"username" description:"..." required:"true"`
	Password     string `json:"password" description:"..." required:"true"`
	TaxNumber    string `json:"tax_number" description:"..." required:"true"`
	Name         string `json:"name" description:"..." required:"true"`
}

func UpdateTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input updateTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		if input.ID != tenant.ID {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.Active = input.Active
		output.EndpointSoap = input.EndpointSoap
		output.EndpointCart = input.EndpointCart
		output.Production = input.Production
		output.Username = input.Username
		output.Password = input.Password
		output.TaxNumber = input.TaxNumber
		output.Name = input.Name

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type patchTenantInput struct {
	TenantId     string  `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Active       *bool   `json:"active" description:"..."`
	EndpointSoap *string `json:"endpoint_soap" description:"..."`
	EndpointCart *string `json:"endpoint_cart" description:"..."`
	Production   *bool   `json:"production" description:"..."`
	Username     *string `json:"username" description:"..."`
	Password     *string `json:"password" description:"..."`
	TaxNumber    *string `json:"tax_number" description:"..."`
	Name         *string `json:"name" description:"..."`
}

func PatchTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input patchTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		output.ID = tenant.ID

		if input.Active == nil {
			output.Active = tenant.Active
		} else {
			output.Active = *input.Active
		}
		if input.EndpointSoap == nil {
			output.EndpointSoap = tenant.EndpointSoap
		} else {
			output.EndpointSoap = *input.EndpointSoap
		}
		if input.EndpointCart == nil {
			output.EndpointCart = tenant.EndpointCart
		} else {
			output.EndpointCart = *input.EndpointCart
		}
		if input.Production == nil {
			output.Production = tenant.Production
		} else {
			output.Production = *input.Production
		}
		if input.Username == nil {
			output.Username = tenant.Username
		} else {
			output.Username = *input.Username
		}
		if input.Password == nil {
			output.Password = tenant.Password
		} else {
			output.Password = *input.Password
		}
		if input.TaxNumber == nil {
			output.TaxNumber = tenant.TaxNumber
		} else {
			output.TaxNumber = *input.TaxNumber
		}
		if input.Name == nil {
			output.Name = tenant.Name
		} else {
			output.Name = *input.Name
		}

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Existing Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type disableTenantInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func DisableTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input disableTenantInput, _ *struct{}) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		tenant.Active = false

		err = StoreTenant(sctx, tenant)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Disable Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}
