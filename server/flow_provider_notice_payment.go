package server

import (
	"crypto/tls"
	"encoding/base64"
	"encoding/xml"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models/depag"
)

type FlowProviderNoticePayment struct {
	Name     string
	Sctx     *ServerContext
	Payment  *models.Payment
	Service  *models.Service
	Tenant   *models.Tenant
	Request  *depag.GeneraAvviso
	Client   *soap.Client
	Depag    depag.Service
	Err      error
	Msg      string
	Status   bool
	Result   *depag.GeneraAvvisoResponse
	Received bool
	Notice   []byte
}

func (flow *FlowProviderNoticePayment) Exec() bool {
	flow.Name = "ProviderNoticePayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderNoticePayment) prepareRequest() bool {
	param := &models.DepagNoticeRequest{
		Iuv: flow.Payment.Payment.IUV,
	}

	xmlParam, err := xml.Marshal(param)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while encoding param"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	encodedParam := base64.StdEncoding.EncodeToString(xmlParam)

	flow.Request = &depag.GeneraAvviso{
		Ente:     flow.Tenant.TaxNumber,
		Servizio: flow.Service.Code,
		Param:    encodedParam,
	}

	return true
}

func (flow *FlowProviderNoticePayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_notice"))

	flow.Client = soap.NewClient(flow.Tenant.EndpointSoap, soap.WithBasicAuth(flow.Tenant.Username, flow.Tenant.Password), soap.WithTLS(&tls.Config{Renegotiation: tls.RenegotiateOnceAsClient}))

	flow.Depag = depag.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Depag.GeneraAvviso(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting notice request"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	encodedParam := flow.Result.Dovuti.Param
	if encodedParam == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	xmlParam, err := base64.StdEncoding.DecodeString(encodedParam)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding param"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	param := &models.DepagNoticeResponse{}
	err = xml.Unmarshal(xmlParam, param)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while unmarshalling param"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	if param.Result != "OK" || param.Data == nil {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	notice := param.Data.Notice
	if notice == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Notice, err = base64.StdEncoding.DecodeString(notice)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding notice"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	flow.Received = true

	return true
}
