package server

import (
	"context"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"github.com/eko/gocache/lib/v4/cache"
	"github.com/getsentry/sentry-go"
	"github.com/graymeta/stow"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/pkgerrors"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

type ServerContext struct {
	ctx                context.Context
	exitFn             context.CancelFunc
	wg                 *sync.WaitGroup
	serverConfig       ServerConfig
	outlog             zerolog.Logger
	errlog             zerolog.Logger
	fileServer         stow.Location
	fileStorage        stow.Container
	tenantsCache       *cache.Cache[*models.Tenant]
	tenantsSync        *sync.RWMutex
	servicesCache      *cache.Cache[*models.Service]
	servicesSync       *sync.RWMutex
	paymentsCache      *cache.LoadableCache[*models.Payment]
	paymentsSync       *sync.RWMutex
	codesGeneratorSync *sync.Mutex
	eventsProducer     *kafka.Producer
}

type processStarterFn func(*ServerContext)

func InitServerContext() *ServerContext {
	ctx, exitFn := context.WithCancel(context.Background())

	wg := &sync.WaitGroup{}

	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
	errlog := zerolog.New(os.Stderr).Level(zerolog.ErrorLevel).With().Str("channel", "err").Logger()

	serverConfig, err := LoadConfig(ctx)
	if err != nil {
		errlog.Fatal().Str("source", "core").Stack().Err(err).Msg("wrong environment configuration")
	}

	var outlogLevel zerolog.Level
	if serverConfig.Debug {
		outlogLevel = zerolog.DebugLevel
	} else {
		outlogLevel = zerolog.InfoLevel
	}
	outlog := zerolog.New(os.Stdout).Level(outlogLevel).With().Str("channel", "out").Logger()

	sctx := &ServerContext{
		ctx:                ctx,
		exitFn:             exitFn,
		wg:                 wg,
		serverConfig:       serverConfig,
		outlog:             outlog,
		errlog:             errlog,
		fileServer:         nil,
		fileStorage:        nil,
		tenantsCache:       nil,
		tenantsSync:        &sync.RWMutex{},
		servicesCache:      nil,
		servicesSync:       &sync.RWMutex{},
		paymentsCache:      nil,
		paymentsSync:       &sync.RWMutex{},
		codesGeneratorSync: &sync.Mutex{},
	}

	if serverConfig.SentryEnabled {
		err = sentry.Init(sentry.ClientOptions{
			Dsn:           serverConfig.SentryToken,
			EnableTracing: true,
			// Set TracesSampleRate to 1.0 to capture 100%
			// of transactions for performance monitoring.
			// We recommend adjusting this value in production,
			TracesSampleRate: 1.0,
			Release:          "v" + VERSION,
		})
		if err != nil {
			sctx.LogCoreFatal().Stack().Err(err).Msg("sentry initialization failed")
		}
	}

	fileServer, fileStorage := connectFileStorage(sctx)
	sctx.fileServer = fileServer
	sctx.fileStorage = fileStorage

	tenantsCache := startTenantsCache(sctx)
	sctx.tenantsCache = tenantsCache

	servicesCache := startServicesCache(sctx)
	sctx.servicesCache = servicesCache

	paymentsCache := startPaymentsCache(sctx)
	sctx.paymentsCache = paymentsCache

	configsPreloader(sctx)

	eventsProducer := startEventsProducer(sctx)
	sctx.eventsProducer = eventsProducer

	return sctx
}

func (sctx *ServerContext) StartProcess(name string, processStarterFn processStarterFn) {
	sctx.wg.Add(1)
	go func() {
		defer sctx.wg.Done()
		sctx.LogCoreDebug().Msgf("%s process started", name)
		processStarterFn(sctx)
		sctx.LogCoreDebug().Msgf("%s process done", name)
	}()
}

func (sctx *ServerContext) Daemonize() {
	termChan := make(chan os.Signal, 1)

	signal.Notify(termChan, syscall.SIGINT, syscall.SIGTERM)

	<-termChan

	sctx.LogCoreDebug().Msg("shutdown signal received")

	sctx.exitFn()
	sctx.wg.Wait()
	sctx.eventsProducer.Flush(15 * 1000)

	if sctx.serverConfig.SentryEnabled {
		sentry.Flush(2 * time.Second)
	}

	sctx.eventsProducer.Close()

	sctx.paymentsCache.Close()

	sctx.fileServer.Close()

	sctx.LogCoreDebug().Msg("all processes done, shutting down")
}

func (sctx *ServerContext) Ctx() context.Context {
	return sctx.ctx
}

func (sctx *ServerContext) ServerConfig() ServerConfig {
	return sctx.serverConfig
}

func (sctx *ServerContext) FileStorage() stow.Container {
	return sctx.fileStorage
}

func (sctx *ServerContext) TenantsCache() cache.Cache[*models.Tenant] {
	return *sctx.tenantsCache
}

func (sctx *ServerContext) TenantsSync() *sync.RWMutex {
	return sctx.tenantsSync
}

func (sctx *ServerContext) ServicesCache() cache.Cache[*models.Service] {
	return *sctx.servicesCache
}

func (sctx *ServerContext) ServicesSync() *sync.RWMutex {
	return sctx.servicesSync
}

func (sctx *ServerContext) PaymentsCache() cache.LoadableCache[*models.Payment] {
	return *sctx.paymentsCache
}

func (sctx *ServerContext) PaymentsSync() *sync.RWMutex {
	return sctx.paymentsSync
}

func (sctx *ServerContext) GenerateCode() string {
	sctx.codesGeneratorSync.Lock()
	defer sctx.codesGeneratorSync.Unlock()
	time.Sleep(250 * time.Millisecond)
	code := time.Now().UnixMilli() / 100
	return strconv.FormatInt(code, 10)
}

func (sctx *ServerContext) EventsProducer() *kafka.Producer {
	return sctx.eventsProducer
}

func (sctx *ServerContext) Done() <-chan struct{} {
	return sctx.ctx.Done()
}

func (sctx *ServerContext) WithTimeout(duration time.Duration) (context.Context, context.CancelFunc) {
	return context.WithTimeout(sctx.ctx, duration)
}

func (sctx *ServerContext) LogKafkaDebug() *zerolog.Event {
	return sctx.outlog.Debug().Str("source", "kafka")
}

func (sctx *ServerContext) LogKafkaInfo() *zerolog.Event {
	return sctx.outlog.Info().Str("source", "kafka")
}

func (sctx *ServerContext) LogKafkaError() *zerolog.Event {
	return sctx.errlog.Error().Str("source", "kafka")
}

func (sctx *ServerContext) LogKafkaFatal() *zerolog.Event {
	return sctx.errlog.Fatal().Str("source", "kafka")
}

func (sctx *ServerContext) LogHttpDebug() *zerolog.Event {
	return sctx.outlog.Debug().Str("source", "http")
}

func (sctx *ServerContext) LogHttpInfo() *zerolog.Event {
	return sctx.outlog.Info().Str("source", "http")
}

func (sctx *ServerContext) LogHttpError() *zerolog.Event {
	return sctx.errlog.Error().Str("source", "http")
}

func (sctx *ServerContext) LogHttpFatal() *zerolog.Event {
	return sctx.errlog.Fatal().Str("source", "http")
}

func (sctx *ServerContext) LogCoreDebug() *zerolog.Event {
	return sctx.outlog.Debug().Str("source", "core")
}

func (sctx *ServerContext) LogCoreError() *zerolog.Event {
	return sctx.errlog.Error().Str("source", "core")
}

func (sctx *ServerContext) LogCoreFatal() *zerolog.Event {
	return sctx.errlog.Fatal().Str("source", "core")
}
