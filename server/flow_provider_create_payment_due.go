package server

import (
	"crypto/tls"
	"encoding/base64"
	"encoding/xml"
	"fmt"
	"strings"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models/depag"
)

type FlowProviderCreatePaymentDue struct {
	Name          string
	Sctx          *ServerContext
	Payment       *models.Payment
	Service       *models.Service
	Tenant        *models.Tenant
	Request       *depag.AggiornaDovuto
	Client        *soap.Client
	Depag         depag.Service
	Err           error
	Msg           string
	Status        bool
	Result        *depag.AggiornaDovutoResponse
	IUV           string
	NoticeCode    string
	TransactionID string
}

func (flow *FlowProviderCreatePaymentDue) Exec() bool {
	flow.Name = "ProviderCreatePaymentDue"

	flow.Status = true &&
		flow.preparePayment() &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderCreatePaymentDue) preparePayment() bool {
	paymentDetailSplit := []*models.PaymentDetailSplit{}

	if flow.Service.Splitted && flow.Service.Split != nil {
		for _, serviceSplit := range flow.Service.Split {
			serviceSplitAmount := serviceSplit.Amount

			paymentSplit := &models.PaymentDetailSplit{
				Code:   serviceSplit.Code,
				Amount: &serviceSplitAmount,
				Meta: &models.PaymentDetailSplitMeta{
					Kind:      serviceSplit.Kind,
					EntryCode: serviceSplit.EntryCode,
					EntryAcc:  serviceSplit.EntryAcc,
				},
			}
			paymentDetailSplit = append(paymentDetailSplit, paymentSplit)
		}
	}

	if flow.Payment.Payment.Split != nil {
		for _, incomingPaymentSplit := range flow.Payment.Payment.Split {
			if incomingPaymentSplit.Amount == nil {
				for index, paymentSplit := range paymentDetailSplit {
					if incomingPaymentSplit.Code == paymentSplit.Code {
						paymentDetailSplit = append(paymentDetailSplit[:index], paymentDetailSplit[index+1:]...)
					}
				}
			} else {
				for _, paymentSplit := range paymentDetailSplit {
					if incomingPaymentSplit.Code == paymentSplit.Code {
						paymentSplit.Amount = incomingPaymentSplit.Amount
					}
				}
			}
		}
	}

	flow.Payment.Payment.Split = paymentDetailSplit

	var totalAmount float64 = 0.0
	for _, paymentSplit := range flow.Payment.Payment.Split {
		totalAmount += *paymentSplit.Amount
	}

	if len(flow.Payment.Payment.Split) > 0 && flow.Payment.Payment.Amount != totalAmount {
		flow.Msg = "total amount and split amounts sum mismatching"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	return true
}

func (flow *FlowProviderCreatePaymentDue) prepareRequest() bool {
	var payerType, payerName string
	if flow.Payment.Payer.Type == "human" {
		payerType = "F"
		payerName = flow.Payment.Payer.Name + " " + flow.Payment.Payer.FamilyName
	} else {
		payerType = "G"
		payerName = flow.Payment.Payer.Name + " " + flow.Payment.Payer.FamilyName
	}

	splitList := []string{}

	if flow.Payment.Payment.Split != nil {
		for _, paymentSplit := range flow.Payment.Payment.Split {

			splitEntry := paymentSplit.Meta.Kind + "|" + paymentSplit.Meta.EntryCode + "|" + paymentSplit.Meta.EntryAcc + "|" + fmt.Sprintf("%.2f", *paymentSplit.Amount)

			splitList = append(splitList, splitEntry)
		}
	}

	split := strings.Join(splitList, "#")

	param := &models.DepagDueRequest{
		PaymentId:                    flow.Payment.ID,
		PayerType:                    payerType,
		PayerTaxIdentificationNumber: flow.Payment.Payer.TaxIdentificationNumber,
		PayerName:                    payerName,
		PayerStreetName:              flow.Payment.Payer.StreetName,
		PayerBuildingNumber:          flow.Payment.Payer.BuildingNumber,
		PayerPostalCode:              flow.Payment.Payer.PostalCode,
		PayerTownName:                flow.Payment.Payer.TownName,
		PayerCountrySubdivision:      flow.Payment.Payer.CountrySubdivision,
		PayerCountry:                 flow.Payment.Payer.Country,
		PayerEmail:                   flow.Payment.Payer.Email,
		Reason:                       flow.Service.Description,
		ExpireAt:                     flow.Payment.Payment.ExpireAt.Time.Format("2006-01-02"),
		NoticeExpireAt:               flow.Payment.Payment.ExpireAt.Time.Format("2006-01-02"),
		Amount:                       flow.Payment.Payment.Amount,
		Payment: &models.DepagDueRequestPayment{
			Id:      "1",
			Service: flow.Service.Code,
			Reason:  flow.Service.Description,
			Amount:  flow.Payment.Payment.Amount,
			Split:   split,
		},
		Metas: []*models.DepagDueRequestMeta{
			{
				Key:   "ANNORIFERIMENTO",
				Value: flow.Payment.CreatedAt.Format("2006"),
			},
		},
	}

	xmlParam, err := xml.Marshal(param)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while encoding param"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	encodedParam := base64.StdEncoding.EncodeToString(xmlParam)

	flow.Request = &depag.AggiornaDovuto{
		Ente:     flow.Tenant.TaxNumber,
		Servizio: flow.Service.Code,
		Param:    encodedParam,
	}

	return true
}

func (flow *FlowProviderCreatePaymentDue) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("create_payment_due"))

	flow.Client = soap.NewClient(flow.Tenant.EndpointSoap, soap.WithBasicAuth(flow.Tenant.Username, flow.Tenant.Password), soap.WithTLS(&tls.Config{Renegotiation: tls.RenegotiateOnceAsClient}))

	flow.Depag = depag.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Depag.AggiornaDovuto(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting due request"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	encodedParam := flow.Result.Dovuti.Param
	if encodedParam == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	xmlParam, err := base64.StdEncoding.DecodeString(encodedParam)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding param"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	param := &models.DepagDueResponse{}
	err = xml.Unmarshal(xmlParam, param)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while unmarshalling param"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	if param.Result != "OK" || param.Data == nil || param.Data.Iuv == "" || param.Data.NoticeCode == "" {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.IUV = param.Data.Iuv
	flow.NoticeCode = param.Data.NoticeCode

	return true
}
