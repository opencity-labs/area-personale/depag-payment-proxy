package server

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

type FlowRequestPaymentUpdate struct {
	Name                      string
	Sctx                      *ServerContext
	PaymentID                 string
	Tenant                    *models.Tenant
	Service                   *models.Service
	FlowProviderVerifyPayment *FlowProviderVerifyPayment
	Err                       error
	Msg                       string
	Status                    bool
	Result                    *models.Payment
}

func (flow *FlowRequestPaymentUpdate) Exec() bool {
	flow.Name = "RequestPaymentUpdate"

	flow.Status = true &&
		flow.findPayment() &&
		flow.findTenant() &&
		flow.findService() &&
		flow.checkPrecondition() &&
		flow.callProvider() &&
		flow.updatePayment()

	return flow.Status
}

func (flow *FlowRequestPaymentUpdate) findPayment() bool {
	paymentsCache := flow.Sctx.PaymentsCache()

	payment, err := paymentsCache.Get(flow.Sctx.Ctx(), flow.PaymentID)
	flow.Result = payment
	if err != nil && (err.Error() == "value not found in store" || err.Error() == "not found") {
		flow.Err = err
		flow.Msg = "payment not found"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get payment by id error for: " + flow.PaymentID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentUpdate) findTenant() bool {
	tenantsCache := flow.Sctx.TenantsCache()

	tenant, err := tenantsCache.Get(flow.Sctx.Ctx(), flow.Result.TenantID)
	flow.Tenant = tenant
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant payment, tenant not found"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get tenant by id error for: " + flow.Result.TenantID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentUpdate) findService() bool {
	servicesCache := flow.Sctx.ServicesCache()

	service, err := servicesCache.Get(flow.Sctx.Ctx(), flow.Result.ServiceID)
	flow.Service = service
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant payment, service not found"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get service by id error for: " + flow.Result.ServiceID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentUpdate) checkPrecondition() bool {
	if !flow.Tenant.Active {
		flow.Msg = "irrilevant payment, tenant not active"
		return false
	}

	if !flow.Service.Active {
		flow.Msg = "irrilevant payment, service not active"
		return false
	}

	if flow.Result.Status != "PAYMENT_PENDING" && flow.Result.Status != "PAYMENT_STARTED" {
		flow.Msg = "irrilevant payment, current status not valid for this action"
		return false
	}

	return true
}

func (flow *FlowRequestPaymentUpdate) callProvider() bool {
	flow.FlowProviderVerifyPayment = &FlowProviderVerifyPayment{
		Sctx:    flow.Sctx,
		Payment: flow.Result,
		Service: flow.Service,
		Tenant:  flow.Tenant,
	}
	if !flow.FlowProviderVerifyPayment.Exec() {
		if flow.FlowProviderVerifyPayment.Msg == "provider say invalid payment" {
			flow.Err = flow.FlowProviderVerifyPayment.Err
			flow.Msg = "irrilevant payment, not payable"
			return false
		}
		flow.Err = flow.FlowProviderVerifyPayment.Err
		flow.Msg = "provider online payment failed"
		return false
	}

	return true
}

func (flow *FlowRequestPaymentUpdate) updatePayment() bool {
	serverConfig := flow.Sctx.ServerConfig()

	now := models.TimeNow()
	flow.Result.UpdatedAt = now
	flow.Result.EventID = uuid.NewV4().String()
	flow.Result.EventCreatedAt = now
	flow.Result.EventVersion = "2.0"
	flow.Result.AppID = serverConfig.AppName + ":" + VERSION
	flow.Result.Links.Update.LastCheckAt = now
	if flow.FlowProviderVerifyPayment.Payed {
		flow.Result.Status = "COMPLETE"
		flow.Result.Payment.PaidAt = flow.FlowProviderVerifyPayment.PaidAt
	}
	if flow.FlowProviderVerifyPayment.Completed {
		flow.Result.Links.Update.NextCheckAt = models.Time{}
	} else {
		flow.Result.Links.Update.NextCheckAt = flow.Result.NextCheck()
	}

	if flow.FlowProviderVerifyPayment.Completed && flow.Result.Payment.ExpireAt.Before(now) {
		flow.Result.Status = "EXPIRED"
		flow.Result.Links.Update.NextCheckAt = models.Time{}
	}

	err := StorePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "storage error"
		return false
	}

	err = ProducePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "kafka producer error"
		return false
	}

	return true
}
