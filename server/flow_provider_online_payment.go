package server

import (
	"bytes"
	"encoding/json"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

type FlowProviderOnlinePayment struct {
	Name    string
	Sctx    *ServerContext
	Payment *models.Payment
	Service *models.Service
	Tenant  *models.Tenant
	Request *http.Request
	Client  *http.Client
	Err     error
	Msg     string
	Status  bool
	Url     string
}

func (flow *FlowProviderOnlinePayment) Exec() bool {
	flow.Name = "ProviderOnlinePayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderOnlinePayment) prepareRequest() bool {
	serverConfig := flow.Sctx.ServerConfig()

	noticeCode := flow.Payment.Payment.NoticeCode

	onlineDataRequest := &models.DepagOnlineRequest{
		PayerEmail: flow.Payment.Payer.Email,
		Payments: []*models.DepagOnlineRequestPayment{
			{
				NoticeCode:              noticeCode,
				TaxIdentificationNumber: flow.Tenant.TaxNumber,
				Amount:                  int32(flow.Payment.Payment.Amount * 100),
				TenantName:              flow.Tenant.Name,
				Description:             flow.Payment.Reason,
			},
		},
		Links: &models.DepagOnlineRequestLinks{
			ReturnOk:     serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID + "?status=ok",
			ReturnCancel: serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID + "?status=cancel",
			ReturnError:  serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID + "?status=error",
		},
	}

	onlineDataRequestJson, err := json.Marshal(onlineDataRequest)
	if err != nil {
		flow.Err = err
		flow.Msg = "json marshal error while preparing online request"
		return false
	}

	apiEndpoint := flow.Tenant.EndpointCart + "/carts"
	onlineReq, err := http.NewRequest("POST", apiEndpoint, bytes.NewReader(onlineDataRequestJson))
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while preparing online request"
		return false
	}
	onlineReq.Header.Add("Content-Type", "application/json")

	flow.Request = onlineReq

	return true
}

func (flow *FlowProviderOnlinePayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_online"))

	flow.Client = &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	onlineRes, err := flow.Client.Do(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting online request"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}
	defer onlineRes.Body.Close()

	if onlineRes.StatusCode != 302 {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	url, err := onlineRes.Location()
	if err != nil {
		flow.Msg = "provider did not give us a URL for payment"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Url = url.String()

	return true
}
