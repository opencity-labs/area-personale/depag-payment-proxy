package server

import (
	"encoding/json"
	"errors"
	"time"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

func StartKafkaServer(sctx *ServerContext) {
	serverConfig := sctx.ServerConfig()

	consumer, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": serverConfig.KafkaBootstrapServers,
		"group.id":          serverConfig.KafkaGroupId,
		"auto.offset.reset": "earliest",
		// "go.delivery.reports":    false,
		"go.logs.channel.enable": true,
		"debug":                  ",",
	})
	if err != nil {
		sctx.LogKafkaFatal().Stack().Err(err).Msg("kafka consumer initialization failed")
	}

	// Drain Kafka service logs without printing them
	go func() {
		sctx.LogKafkaDebug().Msg("kafka logs drainer started")
		logsChan := consumer.Logs()
		for {
			select {
			case <-sctx.Done():
				sctx.LogKafkaDebug().Msg("kafka logs drainer received exit signal, shutting down")
				return
			case <-logsChan:
				// fmt.Println("KAFKA LOG RECEIVED AND DRAINED")
			}
		}
	}()

	consumer.Subscribe(serverConfig.KafkaTopicName, nil)

	for {
		select {
		case <-sctx.Done():
			sctx.LogKafkaDebug().Msg("kafka server received exit signal, shutting down")
			consumer.Close()
			sctx.LogHttpDebug().Msg("kafka server shutted down")
			return

		default:
			msg, err := consumer.ReadMessage(time.Second)

			if err == nil {
				HandleEventMessage(sctx, msg)
			} else if !err.(kafka.Error).IsTimeout() {
				// The client will automatically try to recover from all errors.
				// Timeout is not considered an error because it is raised by
				// ReadMessage in absence of messages.
				sctx.LogKafkaDebug().Msgf("Consumer error: %v (%v)\n", err, msg)
			}
		}
	}
}

func startEventsProducer(sctx *ServerContext) *kafka.Producer {
	serverConfig := sctx.ServerConfig()

	producer, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers":      serverConfig.KafkaBootstrapServers,
		"go.delivery.reports":    false,
		"go.logs.channel.enable": true,
		"debug":                  ",",
	})
	if err != nil {
		sctx.LogKafkaFatal().Stack().Err(err).Msg("kafka producer initialization failed")
	}

	return producer
}

func ProducePayment(sctx *ServerContext, payment *models.Payment) error {
	serverConfig := sctx.ServerConfig()
	eventsProducer := sctx.EventsProducer()

	itemBytes, err := json.Marshal(payment)
	if err != nil {
		return errors.New("invalid data")
	}

	err = eventsProducer.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &serverConfig.KafkaTopicName, Partition: kafka.PartitionAny},
		Value:          itemBytes,
		Key:            []byte(payment.ServiceID),
	}, nil)
	if err != nil {
		return err
	}

	eventsProducer.Flush(250)

	return nil
}
