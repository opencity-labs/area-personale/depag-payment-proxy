package server

import (
	"crypto/tls"
	"encoding/base64"
	"encoding/xml"
	"time"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models/depag"
)

type FlowProviderVerifyPayment struct {
	Name      string
	Sctx      *ServerContext
	Payment   *models.Payment
	Service   *models.Service
	Tenant    *models.Tenant
	Request   *depag.LeggiDovuto
	Client    *soap.Client
	Depag     depag.Service
	Err       error
	Msg       string
	Status    bool
	Result    *depag.LeggiDovutoResponse
	Completed bool
	Payed     bool
	PaidAt    models.Time
	Receipt   []byte
}

func (flow *FlowProviderVerifyPayment) Exec() bool {
	flow.Name = "ProviderVerifyPayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderVerifyPayment) prepareRequest() bool {
	param := &models.DepagVerifyRequest{
		Iuv: flow.Payment.Payment.IUV,
	}

	xmlParam, err := xml.Marshal(param)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while encoding param"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	encodedParam := base64.StdEncoding.EncodeToString(xmlParam)

	flow.Request = &depag.LeggiDovuto{
		Ente:     flow.Tenant.TaxNumber,
		Servizio: flow.Service.Code,
		Param:    encodedParam,
	}

	return true
}

func (flow *FlowProviderVerifyPayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_verify"))

	flow.Client = soap.NewClient(flow.Tenant.EndpointSoap, soap.WithBasicAuth(flow.Tenant.Username, flow.Tenant.Password), soap.WithTLS(&tls.Config{Renegotiation: tls.RenegotiateOnceAsClient}))

	flow.Depag = depag.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Depag.LeggiDovuto(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting verify request"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	encodedParam := flow.Result.Dovuti.Param
	if encodedParam == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	xmlParam, err := base64.StdEncoding.DecodeString(encodedParam)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding param"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	param := &models.DepagVerifyResponse{}
	err = xml.Unmarshal(xmlParam, param)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while unmarshalling param"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	if param.Result != "OK" || param.Data == nil {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	timer.ObserveDuration()

	resultPayment := param.Data.Payment
	if resultPayment == nil || resultPayment.PaidAt == "" {
		flow.Completed = false
		flow.Payed = false

		return true
	}

	paidAt, err := time.Parse("2006-01-02", resultPayment.PaidAt)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while parsing paid_at date"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	flow.Completed = true
	flow.Payed = true
	flow.PaidAt = models.Time{Time: paidAt}

	return true
}
