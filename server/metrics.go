package server

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	MetricsPaymentsReceived = promauto.NewCounter(prometheus.CounterOpts{
		Name: "payments_reveiced",
		Help: "The total number of payments received",
	})
	MetricsPaymentsNew = promauto.NewCounter(prometheus.CounterOpts{
		Name: "payments_new",
		Help: "The total number of new payments received",
	})
	MetricsPaymentsCreated = promauto.NewCounter(prometheus.CounterOpts{
		Name: "payments_created",
		Help: "The total number of payments created",
	})

	MetricsPaymentsValidationError = promauto.NewCounter(prometheus.CounterOpts{
		Name: "payments_validation_error",
		Help: "The total number of payment validation errors",
	})
	MetricsPaymentsInternalError = promauto.NewCounter(prometheus.CounterOpts{
		Name: "payments_internal_error",
		Help: "The total number of payment internal errors",
	})
	MetricsPaymentsProviderError = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "payments_provider_error",
		Help: "The total number of payment provider errors",
	}, []string{"path"})

	MetricsPaymentsProviderLatency = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "payments_provider_latency",
		Help: "Duration of provider requests",
	}, []string{"path"})
)
