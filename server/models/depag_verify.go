package models

import (
	"encoding/xml"
)

type DepagVerifyRequest struct {
	XMLName xml.Name `xml:"LEGGI_DOVUTO"`
	Iuv     string   `xml:"IUV"` // iuv del pagamento
}

type DepagVerifyResponse struct {
	XMLName xml.Name                 `xml:"LEGGI_DOVUTO_RESULT"`
	Result  string                   `xml:"ESITO_OPERAZIONE"`
	Data    *DepagVerifyResponseData `xml:"DATI"`
}

type DepagVerifyResponseData struct {
	Payment *DepagVerifyResponsePayment `xml:"INFO_PAGAMENTO"`
}

type DepagVerifyResponsePayment struct {
	PaidAt string `xml:"DATA_PAGAMENTO"`
}
