package models

type Tenant struct {
	ID           string `json:"id"`
	Active       bool   `json:"active"`
	EndpointSoap string `json:"endpoint_soap"`
	EndpointCart string `json:"endpoint_cart"`
	Production   bool   `json:"production"`
	Username     string `json:"username"`
	Password     string `json:"password"`
	TaxNumber    string `json:"tax_number"` // ente
	Name         string `json:"name"`
}
