package depag

import (
	"context"
	"encoding/xml"
	"time"

	"github.com/hooklift/gowsdl/soap"
)

// against "unused imports"
var _ time.Time
var _ xml.Name

type AnyType struct {
	InnerXML string `xml:",innerxml"`
}

type AnyURI string

type NCName string

type RicercaDovutiSoggetto struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it ricercaDovutiSoggetto"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	CodiceSoggetto string `xml:"codiceSoggetto,omitempty" json:"codiceSoggetto,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type RicercaDovutiSoggettoResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it ricercaDovutiSoggettoResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type Output struct {
	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type LeggiRicevutaTelematica struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it leggiRicevutaTelematica"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type LeggiRicevutaTelematicaResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it leggiRicevutaTelematicaResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type RegistraIncasso struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it registraIncasso"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type RegistraIncassoResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it registraIncassoResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type AggiornaDovuto struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it aggiornaDovuto"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Servizio string `xml:"servizio,omitempty" json:"servizio,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type AggiornaDovutoResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it aggiornaDovutoResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type GeneraLottoIuv struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it generaLottoIuv"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Servizio string `xml:"servizio,omitempty" json:"servizio,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type GeneraLottoIuvResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it generaLottoIuvResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type LeggiDovuto struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it leggiDovuto"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Servizio string `xml:"servizio,omitempty" json:"servizio,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type LeggiDovutoResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it leggiDovutoResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type EliminaDovuto struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it eliminaDovuto"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Servizio string `xml:"servizio,omitempty" json:"servizio,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type EliminaDovutoResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it eliminaDovutoResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type RegistraPagamento struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it registraPagamento"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Servizio string `xml:"servizio,omitempty" json:"servizio,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type RegistraPagamentoResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it registraPagamentoResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type TrattatoDovuto struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it trattatoDovuto"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Servizio string `xml:"servizio,omitempty" json:"servizio,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type TrattatoDovutoResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it trattatoDovutoResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type LeggiRiversamenti struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it leggiRiversamenti"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type LeggiRiversamentiResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it leggiRiversamentiResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type RegistraPagamentoFuoriNodo struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it registraPagamentoFuoriNodo"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Servizio string `xml:"servizio,omitempty" json:"servizio,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type RegistraPagamentoFuoriNodoResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it registraPagamentoFuoriNodoResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type GeneraAvviso struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it generaAvviso"`

	Ente string `xml:"ente,omitempty" json:"ente,omitempty"`

	Servizio string `xml:"servizio,omitempty" json:"servizio,omitempty"`

	Param string `xml:"param,omitempty" json:"param,omitempty"`
}

type GeneraAvvisoResponse struct {
	XMLName xml.Name `xml:"ws.depag.finmatica.it generaAvvisoResponse"`

	Dovuti *Output `xml:"dovuti,omitempty" json:"dovuti,omitempty"`
}

type Service interface {
	RicercaDovutiSoggetto(request *RicercaDovutiSoggetto) (*RicercaDovutiSoggettoResponse, error)

	RicercaDovutiSoggettoContext(ctx context.Context, request *RicercaDovutiSoggetto) (*RicercaDovutiSoggettoResponse, error)

	LeggiRicevutaTelematica(request *LeggiRicevutaTelematica) (*LeggiRicevutaTelematicaResponse, error)

	LeggiRicevutaTelematicaContext(ctx context.Context, request *LeggiRicevutaTelematica) (*LeggiRicevutaTelematicaResponse, error)

	RegistraIncasso(request *RegistraIncasso) (*RegistraIncassoResponse, error)

	RegistraIncassoContext(ctx context.Context, request *RegistraIncasso) (*RegistraIncassoResponse, error)

	AggiornaDovuto(request *AggiornaDovuto) (*AggiornaDovutoResponse, error)

	AggiornaDovutoContext(ctx context.Context, request *AggiornaDovuto) (*AggiornaDovutoResponse, error)

	GeneraLottoIuv(request *GeneraLottoIuv) (*GeneraLottoIuvResponse, error)

	GeneraLottoIuvContext(ctx context.Context, request *GeneraLottoIuv) (*GeneraLottoIuvResponse, error)

	LeggiDovuto(request *LeggiDovuto) (*LeggiDovutoResponse, error)

	LeggiDovutoContext(ctx context.Context, request *LeggiDovuto) (*LeggiDovutoResponse, error)

	EliminaDovuto(request *EliminaDovuto) (*EliminaDovutoResponse, error)

	EliminaDovutoContext(ctx context.Context, request *EliminaDovuto) (*EliminaDovutoResponse, error)

	RegistraPagamento(request *RegistraPagamento) (*RegistraPagamentoResponse, error)

	RegistraPagamentoContext(ctx context.Context, request *RegistraPagamento) (*RegistraPagamentoResponse, error)

	TrattatoDovuto(request *TrattatoDovuto) (*TrattatoDovutoResponse, error)

	TrattatoDovutoContext(ctx context.Context, request *TrattatoDovuto) (*TrattatoDovutoResponse, error)

	LeggiRiversamenti(request *LeggiRiversamenti) (*LeggiRiversamentiResponse, error)

	LeggiRiversamentiContext(ctx context.Context, request *LeggiRiversamenti) (*LeggiRiversamentiResponse, error)

	RegistraPagamentoFuoriNodo(request *RegistraPagamentoFuoriNodo) (*RegistraPagamentoFuoriNodoResponse, error)

	RegistraPagamentoFuoriNodoContext(ctx context.Context, request *RegistraPagamentoFuoriNodo) (*RegistraPagamentoFuoriNodoResponse, error)

	GeneraAvviso(request *GeneraAvviso) (*GeneraAvvisoResponse, error)

	GeneraAvvisoContext(ctx context.Context, request *GeneraAvviso) (*GeneraAvvisoResponse, error)
}

type service struct {
	client *soap.Client
}

func NewService(client *soap.Client) Service {
	return &service{
		client: client,
	}
}

func (service *service) RicercaDovutiSoggettoContext(ctx context.Context, request *RicercaDovutiSoggetto) (*RicercaDovutiSoggettoResponse, error) {
	response := new(RicercaDovutiSoggettoResponse)
	err := service.client.CallContext(ctx, "", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) RicercaDovutiSoggetto(request *RicercaDovutiSoggetto) (*RicercaDovutiSoggettoResponse, error) {
	return service.RicercaDovutiSoggettoContext(
		context.Background(),
		request,
	)
}

func (service *service) LeggiRicevutaTelematicaContext(ctx context.Context, request *LeggiRicevutaTelematica) (*LeggiRicevutaTelematicaResponse, error) {
	response := new(LeggiRicevutaTelematicaResponse)
	err := service.client.CallContext(ctx, "", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) LeggiRicevutaTelematica(request *LeggiRicevutaTelematica) (*LeggiRicevutaTelematicaResponse, error) {
	return service.LeggiRicevutaTelematicaContext(
		context.Background(),
		request,
	)
}

func (service *service) RegistraIncassoContext(ctx context.Context, request *RegistraIncasso) (*RegistraIncassoResponse, error) {
	response := new(RegistraIncassoResponse)
	err := service.client.CallContext(ctx, "", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) RegistraIncasso(request *RegistraIncasso) (*RegistraIncassoResponse, error) {
	return service.RegistraIncassoContext(
		context.Background(),
		request,
	)
}

func (service *service) AggiornaDovutoContext(ctx context.Context, request *AggiornaDovuto) (*AggiornaDovutoResponse, error) {
	response := new(AggiornaDovutoResponse)
	err := service.client.CallContext(ctx, "", request, response)

	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) AggiornaDovuto(request *AggiornaDovuto) (*AggiornaDovutoResponse, error) {
	return service.AggiornaDovutoContext(
		context.Background(),
		request,
	)
}

func (service *service) GeneraLottoIuvContext(ctx context.Context, request *GeneraLottoIuv) (*GeneraLottoIuvResponse, error) {
	response := new(GeneraLottoIuvResponse)
	err := service.client.CallContext(ctx, "", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) GeneraLottoIuv(request *GeneraLottoIuv) (*GeneraLottoIuvResponse, error) {
	return service.GeneraLottoIuvContext(
		context.Background(),
		request,
	)
}

func (service *service) LeggiDovutoContext(ctx context.Context, request *LeggiDovuto) (*LeggiDovutoResponse, error) {
	response := new(LeggiDovutoResponse)
	err := service.client.CallContext(ctx, "", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) LeggiDovuto(request *LeggiDovuto) (*LeggiDovutoResponse, error) {
	return service.LeggiDovutoContext(
		context.Background(),
		request,
	)
}

func (service *service) EliminaDovutoContext(ctx context.Context, request *EliminaDovuto) (*EliminaDovutoResponse, error) {
	response := new(EliminaDovutoResponse)
	err := service.client.CallContext(ctx, "", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) EliminaDovuto(request *EliminaDovuto) (*EliminaDovutoResponse, error) {
	return service.EliminaDovutoContext(
		context.Background(),
		request,
	)
}

func (service *service) RegistraPagamentoContext(ctx context.Context, request *RegistraPagamento) (*RegistraPagamentoResponse, error) {
	response := new(RegistraPagamentoResponse)
	err := service.client.CallContext(ctx, "", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) RegistraPagamento(request *RegistraPagamento) (*RegistraPagamentoResponse, error) {
	return service.RegistraPagamentoContext(
		context.Background(),
		request,
	)
}

func (service *service) TrattatoDovutoContext(ctx context.Context, request *TrattatoDovuto) (*TrattatoDovutoResponse, error) {
	response := new(TrattatoDovutoResponse)
	err := service.client.CallContext(ctx, "", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) TrattatoDovuto(request *TrattatoDovuto) (*TrattatoDovutoResponse, error) {
	return service.TrattatoDovutoContext(
		context.Background(),
		request,
	)
}

func (service *service) LeggiRiversamentiContext(ctx context.Context, request *LeggiRiversamenti) (*LeggiRiversamentiResponse, error) {
	response := new(LeggiRiversamentiResponse)
	err := service.client.CallContext(ctx, "", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) LeggiRiversamenti(request *LeggiRiversamenti) (*LeggiRiversamentiResponse, error) {
	return service.LeggiRiversamentiContext(
		context.Background(),
		request,
	)
}

func (service *service) RegistraPagamentoFuoriNodoContext(ctx context.Context, request *RegistraPagamentoFuoriNodo) (*RegistraPagamentoFuoriNodoResponse, error) {
	response := new(RegistraPagamentoFuoriNodoResponse)
	err := service.client.CallContext(ctx, "", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) RegistraPagamentoFuoriNodo(request *RegistraPagamentoFuoriNodo) (*RegistraPagamentoFuoriNodoResponse, error) {
	return service.RegistraPagamentoFuoriNodoContext(
		context.Background(),
		request,
	)
}

func (service *service) GeneraAvvisoContext(ctx context.Context, request *GeneraAvviso) (*GeneraAvvisoResponse, error) {
	response := new(GeneraAvvisoResponse)
	err := service.client.CallContext(ctx, "", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) GeneraAvviso(request *GeneraAvviso) (*GeneraAvvisoResponse, error) {
	return service.GeneraAvvisoContext(
		context.Background(),
		request,
	)
}
