package models

import "time"

const TimeSecond time.Duration = time.Second
const TimeMinute time.Duration = time.Minute
const TimeHour time.Duration = time.Hour
const TimeDay time.Duration = time.Hour * 24
const TimeYear time.Duration = TimeDay * 365

type Time struct {
	time.Time
}

func (t Time) Add(d time.Duration) Time {
	t2 := t.Time.Add(d)
	return Time{t2}
}

func (t Time) Before(t2 Time) bool {
	return t.Time.Before(t2.Time)
}

func (t Time) MarshalJSON() ([]byte, error) {
	if t.IsZero() {
		return []byte("null"), nil
	} else {
		return t.Time.MarshalJSON()
	}
}

func TimeNow() Time {
	return Time{time.Now()}
}

func TimeDays(days time.Weekday) time.Duration {
	return time.Duration(days) * TimeDay
}
