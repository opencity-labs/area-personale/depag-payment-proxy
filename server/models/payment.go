package models

type Payment struct {
	ID             string        `json:"id"`               // Obbligatorio
	UserID         string        `json:"user_id"`          // Obbligatorio
	Type           string        `json:"type"`             // Obbligatorio, valore permesso: PAGOPA, se non valido si scarta l'evento generando un log di errore
	TenantID       string        `json:"tenant_id"`        // Obbligatorio
	ServiceID      string        `json:"service_id"`       // Obbligatorio
	CreatedAt      Time          `json:"created_at"`       // Obbligatorio, validazione ISO 8601
	UpdatedAt      Time          `json:"updated_at"`       // Obbligatorio, validazione ISO 8601
	Status         string        `json:"status"`           // Obbligatorio, valori permessi: CREATION_PENDING, CREATION_FAILED, PAYMENT_PENDING, PAYMENT_STARTED, PAYMENT_CONFIRMED, PAYMENT_FAILED, NOTIFICATION_PENDING, COMPLETE, EXPIRED
	Reason         string        `json:"reason"`           // Obbligatorio, validazione max 140 caratteri, causale del pagamenti
	RemoteID       string        `json:"remote_id"`        // Obbligatorio
	Payment        PaymentDetail `json:"payment"`          // Obbligatorio
	Links          PaymentLinks  `json:"links"`            // Obbligatorio
	Payer          PaymentPayer  `json:"payer"`            // Obbligatorio
	EventID        string        `json:"event_id"`         // Obbligatorio, mai produrre un evento senza generare un nuovo EventID
	EventVersion   string        `json:"event_version"`    // Obbligatorio, attualmente supportiamo solo eventi con versione 2.0
	EventCreatedAt Time          `json:"event_created_at"` // Obbligatorio, validazione ISO 8601
	AppID          string        `json:"app_id"`           // Obbligatorio, $APP_NAME:$APP_VERSION
}

type PaymentDetail struct {
	TransactionID string                `json:"transaction_id"`                          // Non obbligatorio, fornito dall'intermediario di pagamento
	PaidAt        Time                  `json:"paid_at"`                                 // Non obbligatorio, validazione ISO 8601
	ExpireAt      Time                  `json:"expire_at"`                               // Obbligatorio, validazione ISO 8601, già valorizzato a priori, non viene gestito dal proxy
	Amount        float64               `json:"amount" description:"..." format:"float"` // Obbligatorio, già valorizzato a priori, non viene gestito dal proxy
	Currency      string                `json:"currency"`                                // Obbligatorio, validazione ISO 4217
	NoticeCode    string                `json:"notice_code"`                             // Non obbligatorio
	IUD           string                `json:"iud"`                                     // Obbligatorio
	IUV           string                `json:"iuv"`                                     // Non obbligatorio
	Split         []*PaymentDetailSplit `json:"split"`                                   // Non obbligatorio
}

type PaymentDetailSplit struct {
	Code   string                  `json:"code"`
	Amount *float64                `json:"amount" description:"..." format:"float"`
	Meta   *PaymentDetailSplitMeta `json:"meta"`
}

type PaymentDetailSplitMeta struct {
	Kind      string `json:"kind,omitempty"`
	EntryCode string `json:"entry_code,omitempty"`
	EntryAcc  string `json:"entry_acc,omitempty"`
}

type PaymentLinks struct {
	OnlinePaymentBegin   PaymentLink           `json:"online_payment_begin"`   // Obbligatorio
	OnlinePaymentLanding PaymentLink           `json:"online_payment_landing"` // Obbligatorio
	OfflinePayment       PaymentLink           `json:"offline_payment"`        // Obbligatorio
	Receipt              PaymentLink           `json:"receipt"`                // Obbligatorio
	Notify               []PaymentNotification `json:"notify"`                 // Non obbligatorio
	Update               PaymentUpdate         `json:"update"`                 // Obbligatorio
}

type PaymentLink struct {
	Url          string `json:"url"`            // Non obbligatorio
	LastOpenedAt Time   `json:"last_opened_at"` // Non obbligatorio, validazione ISO 8601
	Method       string `json:"method"`         // Non obbligatorio, valori permessi: GET, POST
}

type PaymentNotification struct {
	Url    string `json:"url"`     // Non obbligatorio
	Method string `json:"method"`  // Non obbligatorio, valori permessi: GET, POST
	SentAt Time   `json:"sent_at"` // Non obbligatorio, validazione ISO 8601
}

type PaymentUpdate struct {
	Url         string `json:"url"`           // Non obbligatorio
	LastCheckAt Time   `json:"last_check_at"` // Non obbligatorio, validazione ISO 8601
	NextCheckAt Time   `json:"next_check_at"` // Non obbligatorio, validazione ISO 8601
	Method      string `json:"method"`        // Non obbligatorio, valori permessi: GET, POST
}

type PaymentPayer struct {
	Type                    string `json:"type"`                      // Obbligatorio, valori permessi: human, legal
	TaxIdentificationNumber string `json:"tax_identification_number"` // Obbligatorio
	Name                    string `json:"name"`                      // Obbligatorio
	FamilyName              string `json:"family_name"`               // Non obbligatorio
	StreetName              string `json:"street_name"`               // Non obbligatorio
	BuildingNumber          string `json:"building_number"`           // Non obbligatorio
	PostalCode              string `json:"postal_code"`               // Non obbligatorio
	TownName                string `json:"town_name"`                 // Non obbligatorio
	CountrySubdivision      string `json:"country_subdivision"`       // Non obbligatorio, validazione ISO 3166-2
	Country                 string `json:"country"`                   // Non obbligatorio, validazione ISO 3166-1 alpha-2
	Email                   string `json:"email"`                     // Non obbligatorio
}

func (payment *Payment) NextCheck() Time {
	createdAt := payment.CreatedAt
	lastCheck := payment.Links.Update.LastCheckAt
	nextCheck := TimeNow()

	minutesCheckpoint := createdAt.Add(5 * TimeMinute)
	quarterCheckpoint := createdAt.Add(15 * TimeMinute)
	weekCheckpoint := createdAt.Add(7 * TimeDay)
	monthCheckpoint := createdAt.Add(30 * TimeDay)
	yearCheckpoint := createdAt.Add(1 * TimeYear)

	if lastCheck.Before(minutesCheckpoint) {
		nextCheck = nextCheck.Add(1 * TimeMinute)
	} else if lastCheck.Before(quarterCheckpoint) {
		nextCheck = nextCheck.Add(5 * TimeMinute)
	} else if lastCheck.Before(weekCheckpoint) {
		nextCheck = nextCheck.Add(1 * TimeHour)
	} else if lastCheck.Before(monthCheckpoint) {
		nextCheck = nextCheck.Add(6 * TimeHour)
	} else if lastCheck.Before(yearCheckpoint) {
		daysAhead := 6 - nextCheck.Weekday()
		if daysAhead <= 0 { // Target day already happened this week
			daysAhead += 7
		}
		nextCheck = nextCheck.Add(TimeDays(daysAhead))
	}

	return nextCheck
}
