package models

import (
	"encoding/xml"
)

type DepagReceiptRequest struct {
	XMLName xml.Name `xml:"LEGGI_RT"`
	Iuv     string   `xml:"IUV"` // iuv del pagamento
}

type DepagReceiptResponse struct {
	XMLName xml.Name                  `xml:"LEGGI_RT_RESULT"`
	Result  string                    `xml:"ESITO_OPERAZIONE"`
	Data    *DepagReceiptResponseData `xml:"DATI"`
}

type DepagReceiptResponseData struct {
	Receipt string `xml:"RT_PDF"`
}
