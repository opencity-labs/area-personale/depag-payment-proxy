package models

type Service struct {
	ID          string         `json:"id"`
	TenantID    string         `json:"tenant_id"`
	Active      bool           `json:"active"`
	Code        string         `json:"code"`        // servizio
	Description string         `json:"description"` // descrizione usata come causale
	Splitted    bool           `json:"splitted"`
	Split       []ServiceSplit `json:"split"`
}

type ServiceSplit struct {
	Code      string  `json:"split_code"`
	Amount    float64 `json:"split_amount" description:"..." format:"float"`
	Kind      string  `json:"split_kind"`       // un'opzione tra 0,1,2,9
	EntryCode string  `json:"split_entry_code"` // codice
	EntryAcc  string  `json:"split_entry_acc"`  // accertamento
}
