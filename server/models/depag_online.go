package models

type DepagOnlineRequest struct {
	PayerEmail string                       `json:"emailNotice"`
	Payments   []*DepagOnlineRequestPayment `json:"paymentNotices"`
	Links      *DepagOnlineRequestLinks     `json:"returnUrls"`
}

type DepagOnlineRequestPayment struct {
	NoticeCode              string `json:"noticeNumber"` // codice avviso pagamento
	TaxIdentificationNumber string `json:"fiscalCode"`   // codice fiscale o partita iva pagatore
	Amount                  int32  `json:"amount"`       // totale pagamento, indicato in centesimi di euro come numero intero
	TenantName              string `json:"companyName"`  // nome ente
	Description             string `json:"description"`  // causale pagamento
}

type DepagOnlineRequestLinks struct {
	ReturnOk     string `json:"returnOkUrl"`     // "{{url landing}}?status=ok"
	ReturnCancel string `json:"returnCancelUrl"` // "{{url landing}}?status=cancel"
	ReturnError  string `json:"returnErrorUrl"`  // "{{url landing}}?status=error"
}
