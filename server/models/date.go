package models

import "time"

type Date struct {
	time.Time
}

func (t *Date) UnmarshalJSON(b []byte) (err error) {
	if string(b) == "null" {
		t.Time = time.Time{}
	} else {
		date, err := time.Parse(`"2006-01-02"`, string(b))
		if err != nil {
			return err
		}
		t.Time = date
	}
	return
}
