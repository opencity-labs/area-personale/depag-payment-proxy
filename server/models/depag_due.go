package models

import "encoding/xml"

type DepagDueRequest struct {
	XMLName                      xml.Name                `xml:"AGGIORNA_DOVUTO"`
	PaymentId                    string                  `xml:"CHIAVE"`              // id del pagamento
	PayerType                    string                  `xml:"TIPO_PAGATORE"`       // "F" fisica o "G" giuridica
	PayerTaxIdentificationNumber string                  `xml:"CODICE_PAGATORE"`     // codice fiscale o partita iva pagatore
	PayerName                    string                  `xml:"ANAGRAFICA_PAGATORE"` // nome e cognome o ragione sociale pagatore
	PayerStreetName              string                  `xml:"INDIRIZZO_PAGATORE"`
	PayerBuildingNumber          string                  `xml:"CIVICO_PAGATORE"`
	PayerPostalCode              string                  `xml:"CAP_PAGATORE"`
	PayerTownName                string                  `xml:"LOCALITA_PAGATORE"`
	PayerCountrySubdivision      string                  `xml:"PROVINCIA_PAGATORE"`
	PayerCountry                 string                  `xml:"NAZIONE_PAGATORE"`
	PayerEmail                   string                  `xml:"EMAIL_PAGATORE"`
	Reason                       string                  `xml:"CAUSALE_VERSAMENTO"`
	ExpireAt                     string                  `xml:"DATA_SCADENZA"`
	NoticeExpireAt               string                  `xml:"DATA_SCADENZA_AVVISO"`
	Amount                       float64                 `xml:"IMPORTO_TOTALE"` // importo totale, diverso da 0.00, indicato con due decimali dopo il punto
	Payment                      *DepagDueRequestPayment `xml:"SINGOLO_VERSAMENTO"`
	Metas                        []*DepagDueRequestMeta  `xml:"METADATA"`
}

type DepagDueRequestPayment struct {
	Id      string  `xml:"PROGRESSIVO"` // sempre 1
	Service string  `xml:"SERVIZIO"`    // codice DePag che identifica il servizio per l’ente
	Reason  string  `xml:"CAUSALE"`
	Amount  float64 `xml:"IMPORTO"`
	Split   string  `xml:"BILANCIO"` // informazioni sul bilancio, dettagli indicati dopo, voce presente solo se bilancio richiesto dal servizio
}

// Sono previsti max 80 dettagli per cui vanno ripetute le 4 informazioni tipo_contabilità|codice|accertamento|importo (accertamento non obbligatorio).
// Il formato prevede | come separatore di campo e # come separatore di dettaglio (es. 9|444||80#9|333|2023/1|20 scompone in 2 dettagli un totale di 100€).
// Il totale importo dei dettagli deve quadrare.

// tipo_contabilità(sempre 9, ma ci sono le opzioni)|codice|accertamento|importo <- poi # per joinare più righe

// le opzioni di tipo_contabilità sono:

// 	0 Capitolo e articolo di Entrata del Bilancio dello Stato
// 	1 Numero della contabilità speciale
// 	2 Codice SIOPE
// 	9 Altro codice ad uso dell’Ente Creditore

type DepagDueRequestMeta struct {
	XMLName xml.Name `xml:"MAPENTRY"`
	Key     string   `xml:"KEY"`   // ANNORIFERIMENTO
	Value   string   `xml:"VALUE"` // anno di riferimento del pagamento
}

type DepagDueResponse struct {
	XMLName xml.Name              `xml:"AGGIORNA_DOVUTO_RESULT"`
	Result  string                `xml:"ESITO_OPERAZIONE"`
	Data    *DepagDueResponseData `xml:"DATI"`
}

type DepagDueResponseData struct {
	Iuv        string `xml:"IUV"`
	NoticeCode string `xml:"CODICE_AVVISO"`
}
