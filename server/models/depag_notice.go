package models

import (
	"encoding/xml"
)

type DepagNoticeRequest struct {
	XMLName xml.Name `xml:"GENERA_AVVISO"`
	Iuv     string   `xml:"IUV"` // iuv del pagamento
}

type DepagNoticeResponse struct {
	XMLName xml.Name                 `xml:"GENERA_AVVISO_RESULT"`
	Result  string                   `xml:"ESITO_OPERAZIONE"`
	Data    *DepagNoticeResponseData `xml:"DATI"`
}

type DepagNoticeResponseData struct {
	Notice string `xml:"AVVISO"`
}
