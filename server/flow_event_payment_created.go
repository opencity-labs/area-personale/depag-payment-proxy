package server

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

type FlowEventPaymentCreated struct {
	Name         string
	Sctx         *ServerContext
	CreationFlow *FlowProviderCreatePaymentDue
	Err          error
	Msg          string
	Result       *models.Payment
}

func (flow *FlowEventPaymentCreated) Exec() bool {
	flow.Name = "EventPaymentCreated"

	status := true &&
		flow.preparePayment() &&
		flow.storePayment() &&
		flow.produceEvent()

	if status {
		MetricsPaymentsCreated.Inc()
	}

	return status
}

func (flow *FlowEventPaymentCreated) preparePayment() bool {
	serverConfig := flow.Sctx.ServerConfig()

	flow.Result = flow.CreationFlow.Payment

	now := models.TimeNow()
	flow.Result.UpdatedAt = now
	flow.Result.EventID = uuid.NewV4().String()
	flow.Result.EventCreatedAt = now
	flow.Result.EventVersion = "2.0"
	flow.Result.AppID = serverConfig.AppName + ":" + VERSION

	if flow.CreationFlow.Status {
		flow.Result.Status = "PAYMENT_PENDING"
		flow.Result.Payment.IUV = flow.CreationFlow.IUV
		flow.Result.Payment.NoticeCode = flow.CreationFlow.NoticeCode
		flow.Result.Payment.TransactionID = flow.CreationFlow.TransactionID

		flow.Result.Links.OnlinePaymentBegin.Url = serverConfig.ExternalApiUrl + serverConfig.BasePath + "online-payment/" + flow.Result.ID
		flow.Result.Links.OnlinePaymentBegin.LastOpenedAt = models.Time{}
		flow.Result.Links.OnlinePaymentBegin.Method = "GET"

		flow.Result.Links.OfflinePayment.Url = serverConfig.ExternalApiUrl + serverConfig.BasePath + "notice/" + flow.Result.ID
		flow.Result.Links.OfflinePayment.LastOpenedAt = models.Time{}
		flow.Result.Links.OfflinePayment.Method = "GET"

		flow.Result.Links.Receipt.Url = serverConfig.ExternalApiUrl + serverConfig.BasePath + "receipt/" + flow.Result.ID
		flow.Result.Links.Receipt.LastOpenedAt = models.Time{}
		flow.Result.Links.Receipt.Method = "GET"

		flow.Result.Links.Update.Url = serverConfig.InternalApiUrl + serverConfig.BasePath + "update/" + flow.Result.ID
		flow.Result.Links.Update.LastCheckAt = now
		flow.Result.Links.Update.NextCheckAt = flow.Result.NextCheck()
		flow.Result.Links.Update.Method = "GET"
	} else {
		flow.Result.Status = "CREATION_FAILED"
	}

	return true
}

func (flow *FlowEventPaymentCreated) storePayment() bool {
	err := StorePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"

		MetricsPaymentsInternalError.Inc()

		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "storage error"

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}

func (flow *FlowEventPaymentCreated) produceEvent() bool {
	err := ProducePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"

		MetricsPaymentsInternalError.Inc()

		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "kafka producer error"

		MetricsPaymentsInternalError.Inc()

		return false
	}

	return true
}
