package server

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

type FlowRequestPaymentReceipt struct {
	Name                       string
	Sctx                       *ServerContext
	PaymentID                  string
	Tenant                     *models.Tenant
	Service                    *models.Service
	FlowProviderReceiptPayment *FlowProviderReceiptPayment
	Err                        error
	Msg                        string
	Status                     bool
	Result                     *models.Payment
	Receipt                    []byte
}

func (flow *FlowRequestPaymentReceipt) Exec() bool {
	flow.Name = "RequestPaymentReceipt"

	flow.Status = true &&
		flow.findPayment() &&
		flow.findTenant() &&
		flow.findService() &&
		flow.checkPrecondition() &&
		flow.callProvider() &&
		flow.updatePayment()

	if flow.Status {
		flow.Receipt = flow.FlowProviderReceiptPayment.Receipt
	}

	return flow.Status
}

func (flow *FlowRequestPaymentReceipt) findPayment() bool {
	paymentsCache := flow.Sctx.PaymentsCache()

	payment, err := paymentsCache.Get(flow.Sctx.Ctx(), flow.PaymentID)
	flow.Result = payment
	if err != nil && (err.Error() == "value not found in store" || err.Error() == "not found") {
		flow.Err = err
		flow.Msg = "payment not found"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get payment by id error for: " + flow.PaymentID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentReceipt) findTenant() bool {
	tenantsCache := flow.Sctx.TenantsCache()

	tenant, err := tenantsCache.Get(flow.Sctx.Ctx(), flow.Result.TenantID)
	flow.Tenant = tenant
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant payment, tenant not found"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get tenant by id error for: " + flow.Result.TenantID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentReceipt) findService() bool {
	servicesCache := flow.Sctx.ServicesCache()

	service, err := servicesCache.Get(flow.Sctx.Ctx(), flow.Result.ServiceID)
	flow.Service = service
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant payment, service not found"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get service by id error for: " + flow.Result.ServiceID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentReceipt) checkPrecondition() bool {
	if !flow.Tenant.Active {
		flow.Msg = "irrilevant payment, tenant not active"
		return false
	}

	if !flow.Service.Active {
		flow.Msg = "irrilevant payment, service not active"
		return false
	}

	return true
}

func (flow *FlowRequestPaymentReceipt) callProvider() bool {
	flow.FlowProviderReceiptPayment = &FlowProviderReceiptPayment{
		Sctx:    flow.Sctx,
		Payment: flow.Result,
		Service: flow.Service,
		Tenant:  flow.Tenant,
	}
	if !flow.FlowProviderReceiptPayment.Exec() {
		if flow.FlowProviderReceiptPayment.Msg == "provider say invalid payment" {
			flow.Err = flow.FlowProviderReceiptPayment.Err
			flow.Msg = "irrilevant payment, not payable"
			return false
		}
		flow.Err = flow.FlowProviderReceiptPayment.Err
		flow.Msg = "provider online payment failed"
		return false
	}

	return true
}

func (flow *FlowRequestPaymentReceipt) updatePayment() bool {
	serverConfig := flow.Sctx.ServerConfig()

	now := models.TimeNow()
	flow.Result.UpdatedAt = now
	flow.Result.EventID = uuid.NewV4().String()
	flow.Result.EventCreatedAt = now
	flow.Result.EventVersion = "2.0"
	flow.Result.AppID = serverConfig.AppName + ":" + VERSION
	flow.Result.Links.Receipt.LastOpenedAt = now
	flow.Result.Links.Update.LastCheckAt = now
	flow.Result.Links.Update.NextCheckAt = flow.Result.NextCheck()

	err := StorePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "storage error"
		return false
	}

	// err = ProducePayment(flow.Sctx, flow.Result)
	// if err != nil && err.Error() == "invalid data" {
	// 	flow.Err = err
	// 	flow.Msg = "json marshal error"
	// 	return false
	// }
	// if err != nil {
	// 	flow.Err = err
	// 	flow.Msg = "kafka producer error"
	// 	return false
	// }

	return true
}
