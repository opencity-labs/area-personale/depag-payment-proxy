package server

import (
	"context"
	"time"

	"github.com/sethvargo/go-envconfig"
)

type ServerConfig struct {
	// Proxy config
	AppName string `env:"APP_NAME,default=depag-payment-proxy"`
	Debug   bool   `env:"DEBUG,default=true"`
	// Sentry config
	SentryEnabled bool   `env:"SENTRY_ENABLED,default=true"`
	SentryToken   string `env:"SENTRY_TOKEN,default=https://c2628ad808302a86c6408da8fc9a9905@o348837.ingest.sentry.io/4505681073668096"`
	// Kafka config
	KafkaBootstrapServers string `env:"KAFKA_BOOTSTRAP_SERVERS,default=localhost:9092"`
	KafkaGroupId          string `env:"KAFKA_GROUP_ID,default=depag_payment_proxy"`
	KafkaTopicName        string `env:"KAFKA_TOPIC_NAME,default=payments"`
	// HTTP config
	HttpBind       string `env:"HTTP_BIND,default=0.0.0.0"`
	HttpPort       string `env:"HTTP_PORT,default=40019"`
	ExternalApiUrl string `env:"EXTERNAL_API_URL,default=https://opencity.app.zoonect.dev"`
	InternalApiUrl string `env:"INTERNAL_API_URL,required"`
	BasePath       string `env:"BASEPATH,default=/payment-proxy/depag/"`
	// Cache config
	CacheExpiration time.Duration `env:"CACHE_EXPIRATION,default=5m"`
	CacheEviction   time.Duration `env:"CACHE_EVICTION,default=10m"`
	// Storage config
	StorageType             string `env:"STORAGE_TYPE,default=s3"` // s3,local,azure
	StorageBucket           string `env:"STORAGE_BUCKET,default=payments"`
	StorageConfigsBasePath  string `env:"STORAGE_CONFIGS_BASE_PATH,default=sdc-payments/depag/tenants/"`
	StoragePaymentsBasePath string `env:"STORAGE_EVENTS_BASE_PATH,default=sdc-payments/depag/payments/"`
	// S3 config
	StorageS3Key      string `env:"STORAGE_S3_KEY,default=02tzwnQ8qYBRFdn1zzI8XwEXAMPLE"`
	StorageS3Secret   string `env:"STORAGE_S3_SECRET,default=02tzwnWAIRCBJwA7HtHUnsEXAMPLEKEY"`
	StorageS3Region   string `env:"STORAGE_S3_REGION,default=local"`
	StorageS3Endpoint string `env:"STORAGE_S3_ENDPOINT,default=localhost:9000"`
	StorageS3Ssl      bool   `env:"STORAGE_S3_SSL,default=false"`
	// Azure config
	StorageAzureAccount string `env:"STORAGE_AZURE_ACCOUNT,default=02tzwnQ8qYBRFdn1zzI8XwEXAMPLE"`
	StorageAzureKey     string `env:"STORAGE_AZURE_KEY,default=02tzwnWAIRCBJwA7HtHUnsEXAMPLEKEY"`
	// Local config
	StorageLocalPath string `env:"STORAGE_LOCAL_PATH,default=/data/payments"`
	// Provider config
	DepagWebhookUser string `env:"DEPAG_WEBHOOK_USER,default=opencontent"`
	DepagWebhookPass string `env:"DEPAG_WEBHOOK_PASS,default=test_opencontent"`
}

func LoadConfig(ctx context.Context) (serverConfig ServerConfig, err error) {
	err = envconfig.Process(ctx, &serverConfig)
	return
}
