package server

import (
	"github.com/eko/gocache/lib/v4/cache"
	goCacheStore "github.com/eko/gocache/store/go_cache/v4"
	goCache "github.com/patrickmn/go-cache"

	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

func startTenantsCache(sctx *ServerContext) *cache.Cache[*models.Tenant] {
	serverConfig := sctx.ServerConfig()

	goCacheClient := goCache.New(goCache.NoExpiration, serverConfig.CacheEviction)
	goCacheStore := goCacheStore.NewGoCache(goCacheClient)

	cacheManager := cache.New[*models.Tenant](goCacheStore)

	return cacheManager
}

func startServicesCache(sctx *ServerContext) *cache.Cache[*models.Service] {
	serverConfig := sctx.ServerConfig()

	goCacheClient := goCache.New(goCache.NoExpiration, serverConfig.CacheEviction)
	goCacheStore := goCacheStore.NewGoCache(goCacheClient)

	cacheManager := cache.New[*models.Service](goCacheStore)

	return cacheManager
}

func startPaymentsCache(sctx *ServerContext) *cache.LoadableCache[*models.Payment] {
	serverConfig := sctx.ServerConfig()

	goCacheClient := goCache.New(serverConfig.CacheExpiration, serverConfig.CacheEviction)
	goCacheStore := goCacheStore.NewGoCache(goCacheClient)

	cacheManager := cache.NewLoadable[*models.Payment](
		getPaymentLoaderFn(sctx),
		cache.New[*models.Payment](goCacheStore),
	)

	return cacheManager
}
