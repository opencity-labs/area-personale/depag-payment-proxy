package server

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"strings"

	"github.com/eko/gocache/lib/v4/cache"
	"github.com/graymeta/stow"
	"github.com/graymeta/stow/azure"
	"github.com/graymeta/stow/local"
	"github.com/graymeta/stow/s3"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

func connectFileStorage(sctx *ServerContext) (stow.Location, stow.Container) {
	serverConfig := sctx.ServerConfig()

	if serverConfig.StorageType != "s3" && serverConfig.StorageType != "azure" && serverConfig.StorageType != "local" {
		sctx.LogCoreFatal().Msg("storage type not supported")
	}

	s3DisableSsl := "false"
	if !serverConfig.StorageS3Ssl {
		s3DisableSsl = "true"
	}

	stowConfig := stow.ConfigMap{
		s3.ConfigAccessKeyID: serverConfig.StorageS3Key,
		s3.ConfigSecretKey:   serverConfig.StorageS3Secret,
		s3.ConfigRegion:      serverConfig.StorageS3Region,
		s3.ConfigEndpoint:    serverConfig.StorageS3Endpoint,
		s3.ConfigDisableSSL:  s3DisableSsl,
		azure.ConfigAccount:  serverConfig.StorageAzureAccount,
		azure.ConfigKey:      serverConfig.StorageAzureKey,
		local.ConfigKeyPath:  serverConfig.StorageLocalPath,
	}

	location, err := stow.Dial(serverConfig.StorageType, stowConfig)
	if err != nil {
		sctx.LogCoreFatal().Stack().Err(err).Msg("storage location initialization failed")
	}

	container, err := location.Container(serverConfig.StorageBucket)
	if err != nil {
		sctx.LogCoreFatal().Stack().Err(err).Msg("storage container initialization failed")
	}

	return location, container
}

func configsPreloader(sctx *ServerContext) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageConfigsBasePath

	currentCursor := stow.CursorStart
	for {
		items, nextCursor, err := fileStorage.Items(basePath, currentCursor, 10)
		if err != nil {
			sctx.LogCoreFatal().Stack().Err(err).Msg("storage loader has failed")
		}

		for _, item := range items {
			name := item.Name()
			switch {
			case strings.HasSuffix(name, ".json") && strings.Contains(name, "tenant_"):
				itemReadCloser, err := item.Open()
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader open error for tenant: " + item.Name())
					continue
				}
				defer itemReadCloser.Close()

				itemBytes, err := io.ReadAll(itemReadCloser)
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader read error for tenant: " + item.Name())
					continue
				}

				var tenant models.Tenant
				err = json.Unmarshal(itemBytes, &tenant)
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader unmarshal error for tenant: " + item.Name())
					continue
				}

				tenantsCache := sctx.TenantsCache()
				tenantsCache.Set(sctx.Ctx(), tenant.ID, &tenant)

			case strings.HasSuffix(name, ".json") && strings.Contains(name, "service_"):
				itemReadCloser, err := item.Open()
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader open error for service: " + item.Name())
					continue
				}
				defer itemReadCloser.Close()

				itemBytes, err := io.ReadAll(itemReadCloser)
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader read error for service: " + item.Name())
					continue
				}

				var service models.Service
				err = json.Unmarshal(itemBytes, &service)
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader unmarshal error for service: " + item.Name())
					continue
				}

				servicesCache := sctx.ServicesCache()
				servicesCache.Set(sctx.Ctx(), service.ID, &service)

			default:
				continue
			}
		}

		currentCursor = nextCursor
		if stow.IsCursorEnd(currentCursor) {
			break
		}
	}
}

func getPaymentLoaderFn(sctx *ServerContext) cache.LoadFunction[*models.Payment] {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()

	basePath := serverConfig.StoragePaymentsBasePath

	paymentLoaderFn := func(ctx context.Context, key any) (*models.Payment, error) {
		paymentID, ok := key.(string)
		if !ok {
			return nil, errors.New("payment loader called with wrong type key, must be a string")
		}

		paymentIDParts := strings.Split(paymentID, "-")
		id := basePath + strings.Join(paymentIDParts, "/") + "/payment_" + paymentID + ".json"

		item, err := fileStorage.Item(id)
		if err != nil {
			return nil, err
		}

		itemReadCloser, err := item.Open()
		if err != nil {
			return nil, err
		}
		defer itemReadCloser.Close()

		itemBytes, err := io.ReadAll(itemReadCloser)
		if err != nil {
			return nil, err
		}

		var payment models.Payment
		err = json.Unmarshal(itemBytes, &payment)
		if err != nil {
			return nil, err
		}

		return &payment, nil
	}

	return paymentLoaderFn
}

func StoreTenant(sctx *ServerContext, tenant *models.Tenant) error {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	tenantsCache := sctx.TenantsCache()

	basePath := serverConfig.StorageConfigsBasePath

	itemBytes, err := json.Marshal(tenant)
	if err != nil {
		return errors.New("invalid data")
	}

	itemName := basePath + tenant.ID + "/tenant_" + tenant.ID + ".json"
	itemContent := string(itemBytes)
	itemSize := int64(len(itemContent))

	itemReader := strings.NewReader(itemContent)

	_, err = fileStorage.Put(itemName, itemReader, itemSize, nil)
	if err != nil {
		return err
	}

	err = tenantsCache.Set(sctx.Ctx(), tenant.ID, tenant)
	if err != nil {
		return err
	}

	return nil
}

func StoreService(sctx *ServerContext, service *models.Service) error {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	servicesCache := sctx.ServicesCache()

	basePath := serverConfig.StorageConfigsBasePath

	itemBytes, err := json.Marshal(service)
	if err != nil {
		return errors.New("invalid data")
	}

	itemName := basePath + service.TenantID + "/service_" + service.ID + ".json"
	itemContent := string(itemBytes)
	itemSize := int64(len(itemContent))

	itemReader := strings.NewReader(itemContent)

	_, err = fileStorage.Put(itemName, itemReader, itemSize, nil)
	if err != nil {
		return err
	}

	err = servicesCache.Set(sctx.Ctx(), service.ID, service)
	if err != nil {
		return err
	}

	return nil
}

func StorePayment(sctx *ServerContext, payment *models.Payment) error {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	paymentsCache := sctx.PaymentsCache()

	basePath := serverConfig.StoragePaymentsBasePath

	itemBytes, err := json.Marshal(payment)
	if err != nil {
		return errors.New("invalid data")
	}

	itemIDParts := strings.Split(payment.ID, "-")
	itemName := basePath + strings.Join(itemIDParts, "/") + "/payment_" + payment.ID + ".json"
	itemContent := string(itemBytes)
	itemSize := int64(len(itemContent))

	itemReader := strings.NewReader(itemContent)

	_, err = fileStorage.Put(itemName, itemReader, itemSize, nil)
	if err != nil {
		return err
	}

	err = paymentsCache.Set(sctx.Ctx(), payment.ID, payment)
	if err != nil {
		return err
	}

	return nil
}
