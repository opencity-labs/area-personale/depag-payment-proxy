package server

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/opencontent/stanza-del-cittadino/depag-payment-proxy/server/models"
)

type FlowRequestPaymentNotice struct {
	Name                      string
	Sctx                      *ServerContext
	PaymentID                 string
	Tenant                    *models.Tenant
	Service                   *models.Service
	FlowProviderNoticePayment *FlowProviderNoticePayment
	Err                       error
	Msg                       string
	Status                    bool
	Result                    *models.Payment
	Notice                    []byte
}

func (flow *FlowRequestPaymentNotice) Exec() bool {
	flow.Name = "RequestPaymentNotice"

	flow.Status = true &&
		flow.findPayment() &&
		flow.findTenant() &&
		flow.findService() &&
		flow.checkPrecondition() &&
		flow.checkExpiration() &&
		flow.callProvider() &&
		flow.updatePayment()

	if flow.Status {
		flow.Notice = flow.FlowProviderNoticePayment.Notice
	}

	return flow.Status
}

func (flow *FlowRequestPaymentNotice) findPayment() bool {
	paymentsCache := flow.Sctx.PaymentsCache()

	payment, err := paymentsCache.Get(flow.Sctx.Ctx(), flow.PaymentID)
	flow.Result = payment
	if err != nil && (err.Error() == "value not found in store" || err.Error() == "not found") {
		flow.Err = err
		flow.Msg = "payment not found"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get payment by id error for: " + flow.PaymentID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentNotice) findTenant() bool {
	tenantsCache := flow.Sctx.TenantsCache()

	tenant, err := tenantsCache.Get(flow.Sctx.Ctx(), flow.Result.TenantID)
	flow.Tenant = tenant
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant payment, tenant not found"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get tenant by id error for: " + flow.Result.TenantID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentNotice) findService() bool {
	servicesCache := flow.Sctx.ServicesCache()

	service, err := servicesCache.Get(flow.Sctx.Ctx(), flow.Result.ServiceID)
	flow.Service = service
	if err != nil && err.Error() == "value not found in store" {
		flow.Err = err
		flow.Msg = "irrilevant payment, service not found"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "get service by id error for: " + flow.Result.ServiceID
		return false
	}

	return true
}

func (flow *FlowRequestPaymentNotice) checkPrecondition() bool {
	if !flow.Tenant.Active {
		flow.Msg = "irrilevant payment, tenant not active"
		return false
	}

	if !flow.Service.Active {
		flow.Msg = "irrilevant payment, service not active"
		return false
	}

	if flow.Result.Status != "PAYMENT_PENDING" && flow.Result.Status != "PAYMENT_STARTED" {
		flow.Msg = "irrilevant payment, current status not valid for this action"
		return false
	}

	return true
}

func (flow *FlowRequestPaymentNotice) checkExpiration() bool {
	serverConfig := flow.Sctx.ServerConfig()

	if flow.Result.Payment.ExpireAt.Before(models.TimeNow()) {
		now := models.TimeNow()
		flow.Result.UpdatedAt = now
		flow.Result.EventID = uuid.NewV4().String()
		flow.Result.EventCreatedAt = now
		flow.Result.EventVersion = "2.0"
		flow.Result.AppID = serverConfig.AppName + ":" + VERSION
		flow.Result.Status = "EXPIRED"
		flow.Result.Links.Update.NextCheckAt = models.Time{}

		err := StorePayment(flow.Sctx, flow.Result)
		if err != nil && err.Error() == "invalid data" {
			flow.Err = err
			flow.Msg = "json marshal error"
			return false
		}
		if err != nil {
			flow.Err = err
			flow.Msg = "storage error"
			return false
		}

		err = ProducePayment(flow.Sctx, flow.Result)
		if err != nil && err.Error() == "invalid data" {
			flow.Err = err
			flow.Msg = "json marshal error"
			return false
		}
		if err != nil {
			flow.Err = err
			flow.Msg = "kafka producer error"
			return false
		}

		flow.Msg = "irrilevant payment, expired"
		return false
	}

	return true
}

func (flow *FlowRequestPaymentNotice) callProvider() bool {
	flow.FlowProviderNoticePayment = &FlowProviderNoticePayment{
		Sctx:    flow.Sctx,
		Payment: flow.Result,
		Service: flow.Service,
		Tenant:  flow.Tenant,
	}
	if !flow.FlowProviderNoticePayment.Exec() {
		if flow.FlowProviderNoticePayment.Msg == "provider say invalid payment" {
			flow.Err = flow.FlowProviderNoticePayment.Err
			flow.Msg = "irrilevant payment, not payable"
			return false
		}
		flow.Err = flow.FlowProviderNoticePayment.Err
		flow.Msg = "provider online payment failed"
		return false
	}

	return true
}

func (flow *FlowRequestPaymentNotice) updatePayment() bool {
	serverConfig := flow.Sctx.ServerConfig()

	now := models.TimeNow()
	flow.Result.UpdatedAt = now
	flow.Result.EventID = uuid.NewV4().String()
	flow.Result.EventCreatedAt = now
	flow.Result.EventVersion = "2.0"
	flow.Result.AppID = serverConfig.AppName + ":" + VERSION
	// flow.Result.Status = "PAYMENT_STARTED"
	flow.Result.Links.OfflinePayment.LastOpenedAt = now
	flow.Result.Links.Update.LastCheckAt = now
	flow.Result.Links.Update.NextCheckAt = flow.Result.NextCheck()

	err := StorePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "storage error"
		return false
	}

	err = ProducePayment(flow.Sctx, flow.Result)
	if err != nil && err.Error() == "invalid data" {
		flow.Err = err
		flow.Msg = "json marshal error"
		return false
	}
	if err != nil {
		flow.Err = err
		flow.Msg = "kafka producer error"
		return false
	}

	return true
}
